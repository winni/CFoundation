//
// Created by 叶青 on 2017/3/3.
//

#ifndef KDTEST_KDPREFIXHEADER_H
#define KDTEST_KDPREFIXHEADER_H


#define __DEBUG__

#ifdef __DEBUG__

#define KDLog(format,...)   fprintf(stdout,"[%s|%s] " format "\n",__TIME__, __FUNCTION__, ##__VA_ARGS__)

#define KDErrorLog(format,...)   fprintf(stderr,"[%s|%s] " format "\n",__TIME__, __FUNCTION__, ##__VA_ARGS__)

#define KDDealloc()         fprintf(stdout,"[%s] %s dealloc\n",__TIME__, __FUNCTION__)

#else

#define KDLog(format,...)
#define KDErrorLog(format,...)
#define KDDealloc()

#endif

#endif //KDTEST_KDPREFIXHEADER_H
