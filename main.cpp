
//#include "Foundation/KDFoundationTest.h"
//#include "Mat/KDMatTest.h"
//#include "Mat/KDMatErgodic.h"
//#include "Mat/KDMatMaskOperate.h"
//#include "Mat/KDMatSuperPosition.h"
//#include "Mat/KDMatAlpha.h"
//#include "Mat/ZHCTableScaner.h"
#include "Mat/MDScaleScaner.h"
#include "Utils/KDMatHelp.h"


//#include <unistd.h>

int main() {

//    KDFoundationTest foundationTest;
//    foundationTest.testWhile();
//    foundationTest.testCin();
//    foundationTest.testString();
//    foundationTest.testClass();
//    foundationTest.testPointAndRefrence();
//    foundationTest.testVoidPoint();
//    foundationTest.testPointRefrence();
//    foundationTest.testPointConst();
//    foundationTest.testConstPoint();
//    foundationTest.testDeclType();
//    foundationTest.testVector();
//    foundationTest.testArray();
//    foundationTest.testCharFun();
//    foundationTest.testArrays();
//    foundationTest.testLinkList();
//    foundationTest.testType();

#pragma mark - mat 基础操作
//    std::string path = "./Res/child_small.png";
//    KDMatTest *matTest = new KDMatTest(path);
    //裁剪
//    matTest->cut();
    //浅拷贝
//    matTest->shallowCopy();
    //深拷贝
//    matTest->deepCopy();
    //release
//    matTest->release();
//    matTest->showNewMat();
//    delete(matTest);

#pragma mark - mat 像素遍历
//    std::string path = "./Res/child_small.jpg";
//    KDMatErgodic *ergodicObj = new KDMatErgodic(path);
//    ergodicObj->scanImg();

#pragma mark - mat 掩码操作
//    std::string path = "./Res/child_small.jpg";
//    KDMatMaskOperate *matMaskObj = new KDMatMaskOperate(path);
//    matMaskObj->maskOperation();
//    delete(matMaskObj);

#pragma mark - 多个 mat 叠加
//    KDMatSuperPosition *matSP = new KDMatSuperPosition();
//    matSP->superPosition();
//    delete(matSP);
//#pragma mark - mat 透明度和对比度
//    KDMatAlpha *matAlpha = new KDMatAlpha();
//    matAlpha->changeAlpha();
//    delete(matAlpha);

#pragma mark - 表格扫描
//    std::string path = "./Res/WechatIMG72.jpeg";
//    ZHCTableScaner *obj = new ZHCTableScaner();
//    obj->startScan(path);
//    obj->drawContours();
//    KDMatShowImg(obj->p_resultMat);
//    obj->clean();

#pragma mark - 量表识别
    std::string path = "./Res/IMG_3140.JPG";
    ZHScaner::MDScaleScaner *obj = new ZHScaner::MDScaleScaner();
//    obj->optionSelectedScale = 0.8;
    obj->questionCount = 211;
    obj->enableDebug = true;
    obj->startScan(path);

    cv::Mat result = obj->drawContours();
    KDMatShowImg("result", result);
    KDMatShowImg("name", obj->nameMat);
    obj->clean();



    return 0;

}


