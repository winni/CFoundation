//
// Created by 叶青 on 2016/12/14.
//

#include "KDTestModel.h"
#import <iostream>


KDPeople::KDPeople(void){
        this->age=10;
        this->sex="man";
        this->asset="1000W";
}

KDPeople::KDPeople(int ageV,string sexV,string assetV){
    this->age=ageV;
    this->sex=sexV;
    this->asset=assetV;
}


KDPeople::KDPeople(const KDPeople & c){
    this->age=c.age;
    this->sex=c.sex;
    this->asset=c.asset;
}

KDPeople &KDPeople::operator=(const KDPeople &peo){
    if(&peo==NULL){
        return *this;
    }
    if(this==&peo){
        return *this;
    }
    this->age=peo.age;
    this->sex=peo.sex;
    this->asset=peo.asset;
    return  *this;
}

KDPeople::~KDPeople(void){
  cout << "KDPeople dealloc" << endl;
}