//
// Created by 叶青 on 2016/12/14.
//

#ifndef KDTEST_KDTESTMODEL_H
#define KDTEST_KDTESTMODEL_H

#include <string>
using namespace std;


class KDPeople {

private:
    string asset;


public:
    int age;
    string sex;

    KDPeople(void);

    KDPeople(int ageV,string sexV,string assetV);

    KDPeople(const KDPeople & c);

    KDPeople &operator=(const KDPeople &peo);

    ~KDPeople(void);
};


#endif //KDTEST_KDTESTMODEL_H
