//
// Created by 叶青 on 2016/12/16.
// 公共帮助类

#ifndef KDTEST_KDCOMMONHELP_H
#define KDTEST_KDCOMMONHELP_H


#pragma mark - 耗时时间
/*记录开始时间*/
const double KDBeginTime();
/*输出耗时时间*/
void KDPrintTime(const double beginTime);


#endif //KDTEST_KDCOMMONHELP_H
