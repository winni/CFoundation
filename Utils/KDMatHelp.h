//
// Created by 叶青 on 2017/3/2.
//

#ifndef KDTEST_KDMATHELP_H
#define KDTEST_KDMATHELP_H

#include <opencv2/opencv.hpp>

/*显示图像*/
void KDMatShowImg(cv::Mat mat);
void KDMatShowImg(char *title, cv::Mat mat);
void KDMatShowImgs(cv::Mat *mat, ...);

#pragma mark - 耗时时间
/*记录开始时间,返回毫秒*/
const double KDMatBeginTime();
/*输出耗时时间*/
void KDMatPrintTime(const double beginTime);

/*遍历mat 矩阵点*/
void KDMatPointEnum(cv::Mat *mat,const std::function<void (bool *, int, int, uchar *)> enumAction);

#endif //KDTEST_KDMATHELP_H
