//
// Created by 叶青 on 2017/3/2.
//

#include <opencv2/highgui.hpp>
#include <string>
#include <iostream>

using namespace std;
using namespace cv;

void KDMatShowImgNotWait(cv::Mat *mat);

void KDMatShowImg(cv::Mat mat)
{
    KDMatShowImgNotWait(&mat);
    waitKey();
}

void KDMatShowImg(char *title, cv::Mat mat)
{
    if(mat.data == NULL){
        printf("show error,mat is null\n" );
        return ;
    }
    char winName[20];
    sprintf(winName, title);
    string winNameNew = string(winName);
    namedWindow(winNameNew, WINDOW_AUTOSIZE );
    imshow(winName, mat);
    waitKey();
}

void KDMatShowImgs(cv::Mat *mat, ...)
{
    KDMatShowImgNotWait(mat);
    va_list mats;
    va_start(mats, mat);
    Mat *matBegin=va_arg(mats, Mat*);
    while(matBegin){
        KDMatShowImgNotWait(matBegin);
        matBegin = va_arg(mats, Mat*);
    }
    va_end(mats);
    waitKey();
}

void KDMatShowImgNotWait(cv::Mat *mat)
{
    if(!mat->data){
        printf("show error,mat is null\n" );
        return ;
    }
    char winName[20];
    sprintf(winName,"Mat:%p",mat);
    string winNameNew = string(winName);
    namedWindow(winNameNew, WINDOW_AUTOSIZE );
    imshow(winName, *mat);
}

const double KDMatBeginTime()
{
    const double beginTime = (double)getTickCount();
    return beginTime;
}
void KDMatPrintTime(const double beginTime)
{
    const double consumTime = 1000*((double)getTickCount() - beginTime)/getTickFrequency();
//    printf("[%s] 耗时:%lf milliseconds\n",__TIME__,consumTime);
    cout<<"耗时:"<<consumTime<<"ms"<<endl;
}

void KDMatPointEnum(Mat *mat,const function<void (bool *, int, int, uchar *)> enumAction)
{
    CV_Assert((*mat).depth() != sizeof(uchar));
    int channels = (*mat).channels();
    int nRows = (*mat).rows ;//* channels;
    int nCols = (*mat).cols *channels;
    if ((*mat).isContinuous()) {
        nCols *= nRows;
        nRows = 1;
    }
    int i,j;
    uchar* point;
    bool canStopValue = false;
    bool  *canStop = &canStopValue;
    for( i = 0; i < nRows; ++i) {
        point = (*mat).ptr<uchar>(i);
        for ( j = 0; j < nCols; ++j) {
            enumAction(canStop,i,j,&(point[j]));
            //判断是否跳出循环
            if(*canStop){
                break;
            }
        }
        //判断是否跳出循环
        if(*canStop){
            break;
        }
    }
}