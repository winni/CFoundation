//
// Created by 叶青 on 2017/3/3.
//

#include <sys/time.h>
#include <stdio.h>


const double KDBeginTime()
{
    struct timeval tv;
    gettimeofday(&tv,NULL);
    return tv.tv_sec * 1000 + tv.tv_usec / 1000;
}
void KDPrintTime(const double beginTime)
{
    const double endTime = KDBeginTime();
    const double consumTime = endTime - beginTime;
    printf("耗时:%lf milliseconds\n",consumTime);
}
