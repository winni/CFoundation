//
// Created by YeQing on 2018/11/14.
//

#include "ZHCTableScaner.h"

#include "opencv2/imgproc/imgproc.hpp"

#include <iostream>
#include <math.h>
#include <string.h>


#include "../Utils/KDMatHelp.h"
#include "../Define/KDPrefixHeader.h"


#define kKDTableColumns  6   //6列
#define kKDTableRows     15  //15行
#define kKDRectAreaDeviationRatio   0.4//矩形区域面积偏差比例,用来判断是否大矩形,因为拍摄出来后，矩形轮廓会有扭曲，面积有偏差
#define kKDRectAreaValidWidthRatio  0.82//矩形区域有效范围的宽度比例,后面的填写其他文字,不识别
#define kKDOptionAreaBlackRatio     0.7//选项区域涂黑比例,用来判断选项是否涂黑
#define kKDSampleRectAreaHeight     165//矩形区域 示例高度
#define kKDSampleOptionAreaHeight   30//选项区域 示例高度


using namespace cv;
using namespace std;


#pragma mark - utils

/**
 比较vector里的子选项轮廓

 @param a a description
 @param b b description
 @return return value description
 */
bool zhc_compareOptionContour(const KDOptionNode &a, const KDOptionNode &b)
{
    if (a.isSecondL && !b.isSecondL) {
        //a是第二行,b是第一行
        return false;
    } else if (!a.isSecondL && b.isSecondL) {
        //a是第一行,b是第二行
        return true;
    } else {
        //两个在同一行
        if (a.centerX <= b.centerX) {
            return true;
        } else {
            return false;
        }
    }
}


#pragma mark - Class ZHCTableScaner
ZHCTableScaner::ZHCTableScaner()
{
    p_loopCount = 0;
    p_imgRatio = 1.0;
    p_optionH = 0;
}

/** 析构函数 */
ZHCTableScaner::~ZHCTableScaner()
{
    cout<<"[ZHCTableScaner]~"<<endl;
}

/** 根据图片路径,开始扫描 */
void ZHCTableScaner::startScan(std::string rPath)
{
    Mat fileMat = imread(rPath, CV_LOAD_IMAGE_COLOR);
    this->startScan(fileMat);
}

/** 从根据mat对象,开始扫描 */
void ZHCTableScaner::startScan(cv::Mat inputMat)
{
    this->clean();
#ifdef kZHCTableScaner_EnableLog
    long long begintime = clock();//记录开始时间
#endif
    //初始化mat
    p_originalMat = inputMat;
//    p_originalMat = imread(p_rPath, CV_LOAD_IMAGE_COLOR);
    Mat tmpMat;
    p_resultMat = p_originalMat;
    //中值滤波
    medianBlur(p_resultMat, tmpMat, 1);
    p_resultMat = tmpMat;
    //灰度图像
    cvtColor(p_resultMat, tmpMat, COLOR_BGR2GRAY);
    p_resultMat = tmpMat;
    //二值化,THRESH_BINARY_INV: (src(x,y)>thresh)?0:maxval
    threshold(p_resultMat, tmpMat, 150, 255, THRESH_BINARY_INV);
    p_resultMat = tmpMat;

    //    //Canny边缘检测
    //    Canny(resultMat, tmpMat, 140, 180, 3);
    //    resultMat = tmpMat;

    //查找最大外轮廓
    this->findOuterContour(p_resultMat, &tmpMat);
    p_resultMat = tmpMat;
    //查找所有的矩形轮廓
    this->findRectContours(p_resultMat);
    if (p_areaList.size()<=0) {
        cout<<"[ZHCTableScaner]没有找到区域"<<endl;
        return;
    }
    //矩形轮廓排序
    this->sortRectContours(p_resultMat);

#ifdef kZHCTableScaner_EnableLog
    //打印时间
    long long endtime = clock();//计时结束
    printf("[ZHCTableScaner]总共循环%lu次,耗时%fs\n", p_loopCount, (double)(endtime-begintime)/CLOCKS_PER_SEC);
#endif
}

/*查找最大外轮廓*/
void ZHCTableScaner::findOuterContour(cv::Mat inputImg, cv::Mat *outputImg)
{
    //查找最大外轮廓
    vector<Vec4i> outerAreaHierarchy;
    std::vector<std::vector<cv::Point> > outerContours;
    findContours(inputImg, outerContours, outerAreaHierarchy, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE);
    size_t outerContourCount  = outerContours.size();
#ifdef kZHCTableScaner_EnableLog
    cout<<"[ZHCTableScaner]找到外轮廓:"<<outerContourCount<<"个,开始筛选..."<<endl;
#endif
    //遍历所有的轮廓，查找矩形轮廓
    double maxContourSize = 0;
    Mat maxContour;
    for( size_t i = 0; i < outerContourCount; i++ ) {
#ifdef kZHCTableScaner_EnableLog
        p_loopCount ++;
#endif
        Mat contourMatTmp(outerContours[i]);
        double contourSize = fabs(contourArea(contourMatTmp));
        if (contourSize>maxContourSize) {
            maxContourSize = contourSize;
            maxContour = contourMatTmp;
        }
    }
    if (maxContour.data != NULL) {
        cv::Rect outputRect = boundingRect(maxContour);
        *outputImg = inputImg(outputRect);
        p_originalMat = p_originalMat(outputRect);//截取最大外轮廓
        p_outerContourArea = maxContourSize;//总面积
        p_imgRatio = (outputRect.height*1.0/(kKDTableRows+0.5))/kKDSampleRectAreaHeight;//图像比例
        p_optionH = ceil(kKDSampleOptionAreaHeight*p_imgRatio);//选项轮廓底图实际高度
    }
}

/*查询内部矩形轮廓*/
void ZHCTableScaner::findRectContours(cv::Mat inputImg)
{
    //求平均面积
    const double averageArea = p_outerContourArea*1.0/((kKDTableColumns+1)*(kKDTableRows+0.5));
    const double offsetArea = averageArea*kKDRectAreaDeviationRatio;//30个点的面积误差
    //查找轮廓
    vector<Vec4i> hierarchys;
    //tree模式,index:后、前、子、父
    findContours(inputImg, p_contours, hierarchys, RETR_TREE, CHAIN_APPROX_SIMPLE);
    size_t contourCount  = p_contours.size();
#ifdef kZHCTableScaner_EnableLog
    cout<<"[ZHCTableScaner]找到矩形轮廓:"<<contourCount<<"个,开始筛选..."<<endl;
#endif
    //遍历所有的轮廓，查找矩形轮廓
    for( size_t index = 0; index < contourCount; index++ ) {
#ifdef kZHCTableScaner_EnableLog
        p_loopCount ++;
#endif
        vector<Point> currentContour = p_contours[index];
        Mat contoursTmp(currentContour);
        vector<Point> area;
        approxPolyDP(contoursTmp, area, arcLength(contoursTmp, true) * 0.02, true);
        Mat areaMat(area);
        //没有子轮廓，过滤
        Vec4i hierarchy = hierarchys[index];
        if (hierarchy[2]<0) {
            //找不到子轮廓，可能是小轮廓,这里检测一下是否是 选项小轮廓
            this->findSubOptionContour(areaMat, currentContour ,(int)index, -1, hierarchys);
            continue;
        }
        //检测边数
        if (area.size() <4 ) {
            //边数目过滤，可能是小轮廓,这里检测一下是否是 选项小轮廓
            this->findSubOptionContour(areaMat, currentContour ,(int)index, -1, hierarchys);
            continue;
        }
        //检测面积
        double areaArea = fabs(contourArea(areaMat));
        double diffArea = areaArea - averageArea;
        if (fabs(diffArea)>=offsetArea) {//&& isContourConvex(areaMat)
            //面积不符合矩形轮廓，可能是小轮廓,这里检测一下是否是 选项小轮廓
            if (diffArea<0) {//轮廓面积<平均矩形面积
                this->findSubOptionContour(areaMat, currentContour, (int)index, areaArea, hierarchys);
            }
            continue;
        }
        //过滤第一行第一列，即行头、列头。
        Rect contourRect = this->getRectContourRect((int)index);//取出父轮廓的rect
        if (contourRect.x>=0 && contourRect.x<contourRect.width*0.2) {
            //过滤 列头的轮廓
            continue;
        }
        if (contourRect.y>0 && contourRect.y<contourRect.height*0.2) {
            //过滤 行头的轮廓
            continue;
        }
#ifdef kZHCTableScaner_EnableLog
        //存储 矩形轮廓
        p_rectContours.push_back(area);
#endif
        KDRectNode node = {(int)index};
        //计算rect
        node.x = contourRect.x;
        node.y = contourRect.y;
        node.width = contourRect.width;
        node.height = contourRect.height;
        //找到中心点
        Moments areaM = moments(currentContour);
        float centerX = float(areaM.m10 / areaM.m00);
        float centerY = float(areaM.m01 / areaM.m00);
        node.centerX = centerX;
        node.centerY = centerY;
        node.distance = sqrt(centerX*centerX+centerY*centerY);
        //如果数组为空
        if (p_areaList.size()<=0) {
            p_areaList.push_back(node);
            continue ;
        }
        //判断和第一个点的对角线距离，升序排列，这里不具体排序，只选取出最左上角的节点
        KDRectNode firstArea = p_areaList[0];
        if (node.distance<=firstArea.distance) {
            //要插入到节点 距离原点更近
            p_areaList.insert(p_areaList.begin(), node);
        } else {
            p_areaList.push_back(node);
        }
    }
#ifdef kZHCTableScaner_EnableLog
    cout<<"[ZHCTableScaner]筛选矩形轮廓完成:"<<p_areaList.size()<<"个"<<endl;
#endif
}

/*判断是否是 选项子轮廓*/
void ZHCTableScaner::findSubOptionContour(Mat inputImg, vector<Point> contour, int index, double area, vector<Vec4i> hierarchys)
{
    Vec4i hierarchy = hierarchys[index];
    int superIndex = hierarchy[3];//父节点
    if (superIndex<0) {
        //过滤父节点
        return;
    }
    if (area<=-1) {
        //计算面积
        area = fabs(contourArea(inputImg));
    }
    if (area < 100*p_imgRatio || area > 2000*p_imgRatio) {
        //过滤面积不合法的
        return;
    }
    KDOptionNode optionNode = {index};
    //找到中心点
    Moments areaM = moments(contour);
    optionNode.centerX = float(areaM.m10 / areaM.m00);
    optionNode.centerY = float(areaM.m01 / areaM.m00);
    //rect 边界过滤,因为矩形框内，后面一部分是写文字的区域,但是不需要识别出来。
    Rect contourRect = boundingRect(contour);
    Rect superContourRect = this->getRectContourRect(superIndex);//取出父轮廓的rect
    //计算有效的origin.x位置，对比当前轮廓是不是在这个范围内
    float superValidX = superContourRect.x+superContourRect.width*kKDRectAreaValidWidthRatio;
    if (contourRect.x+contourRect.width>superValidX) {
        //轮廓的right不在有效范围内
        return;
    }
    //当前轮廓在有效范围内
    optionNode.x = contourRect.x;
    optionNode.y = contourRect.y;
    optionNode.width = contourRect.width;
    optionNode.height = contourRect.height;
    //计算是否在第二行，用来排序使用
    if ((optionNode.centerY-superContourRect.y)<=kKDSampleRectAreaHeight*this->p_imgRatio*0.5) {
        //认为在矩形轮廓中心点以上的都在第一行
        optionNode.isSecondL = false;
    } else {
        optionNode.isSecondL = true;
    }
    //计算是否涂黑
    Mat contourMat = p_resultMat(contourRect);
    int rowCount = contourMat.rows;//实际行
    int colCount = contourMat.cols;//实际列
    int blackW = (colCount<p_optionH)?colCount:p_optionH;//涂黑参照 宽
    int blackH = (rowCount<p_optionH)?rowCount:p_optionH;//涂黑参照 高
    long sumBlackPoint = blackW*blackH*kKDOptionAreaBlackRatio;//涂黑参照 标准，黑色点总数
    int blackCount = 0;
    bool isBlack = false;//是否检测到黑点
    for (int x = 0; x < rowCount; x++) {
        for (int y = 0; y < colCount; y++) {
            uchar pointColor = contourMat.at<uchar>(x,y);
            if (pointColor >=254) {//二值化后，涂黑的变成白色，这里判断255,255,255
                blackCount ++;
            }
            //获取临时的涂黑比例
            if (blackCount>=sumBlackPoint) {
                //达到涂黑标准，则不需要继续遍历
                isBlack = true;
                break;
            }
        }
        if (isBlack) {
            //达到涂黑标准，则不需要继续遍历 该区域
            break;
        }
    }
    //涂黑的区域占比
    optionNode.isBlack = isBlack;
    //保存 选项节点
    vector<KDOptionNode> *optionContours = &(p_optionContourMap[superIndex]);
    (*optionContours).push_back(optionNode);
}

/*矩形轮廓排序*/
void ZHCTableScaner::sortRectContours(cv::Mat inputImg)
{
    //遍历找出所有的 行头
    for (int row = 0; row < kKDTableRows; row++) {
        int headNodeIndex = -1;
        KDRectNode headNode;
        if (row==0){
            //第一行，直接取 p_areaList[0]
#ifdef kZHCTableScaner_EnableLog
            p_loopCount ++;
#endif
            headNodeIndex = 0;
            headNode = p_areaList[0];
        } else {
            //不是第一行
            KDRectNode lastArea = p_areaData[p_areaData.size()-1][0];
            float minDiffY = lastArea.height*1.2;//两个点Y距离,这里预留20%的像素的差值，理论上，每两行间距=行高
            const float minDiffX = lastArea.width*0.1;
            for (int i = 0; i < p_areaList.size(); ++i) {
#ifdef kZHCTableScaner_EnableLog
                p_loopCount ++;
#endif
                KDRectNode nodeTmp = p_areaList[i];
                float diffX = nodeTmp.x-lastArea.x;
                float diffY = nodeTmp.y-lastArea.y;
                if (fabs(diffX)<minDiffX && diffY>0 && diffY<=minDiffY) {
                    //找 x差值在20以内，y距离最小的
                    minDiffY = diffY;
                    headNodeIndex = i;
                    headNode = nodeTmp;
                }
            }
        }
        if (headNodeIndex<0) {
            //找不到行头，不继续找后面的列，也不继续找下面的行
            cout<<"[ZHCTableScaner]未找到行头:"<<row<<endl;
            break;
        }
        //找到行 头部列
        vector<KDRectNode > nextArea;
        nextArea.push_back(headNode);
        p_areaData.push_back(nextArea);
        p_areaList.erase(p_areaList.begin()+headNodeIndex);//删除已经找到的节点

        //子选项轮廓排序
        this->sortSubOptionContours(headNode.index);//遍历该矩形轮廓，排序子选项轮廓
        //拼接扫描结果：row开始+开头cell
        this->joinScanResultString(true, false, headNode.index, false);

        //继续找当前行 后面的列，从第二列开始查找
        vector<KDRectNode >* areaRows = &p_areaData[row];
        for (int col = 1; col < kKDTableColumns; col++) {
            int areaRowSize = (int)areaRows->size();
            KDRectNode rowLastNode = (*areaRows)[areaRowSize-1];
            float minDiffX = rowLastNode.width*1.2;//两个点X距离,这里预留20%的像素的差值，理论上，每两列间距=列宽
            const float maxDiffY = rowLastNode.height*0.1;
            int findNodeIndex = -1;
            KDRectNode findNode;
            for (int i = 0; i < p_areaList.size(); ++i) {
#ifdef kZHCTableScaner_EnableLog
                p_loopCount ++;
#endif
                KDRectNode nodeTmp = p_areaList[i];
                float diffX = nodeTmp.x-rowLastNode.x;
                float diffY = nodeTmp.y-rowLastNode.y;
                if (fabs(diffY)<maxDiffY && diffX>0 && diffX<=minDiffX) {
                    //找 y差值在20以内，x距离最小的
                    minDiffX = diffX;
                    findNodeIndex = i;
                    findNode = nodeTmp;
                }
            }
            if (findNodeIndex>=0) {
                //找到后面的列，存储
                (*areaRows).push_back(findNode);
                p_areaList.erase(p_areaList.begin()+findNodeIndex);//删除已经找到的节点
                //遍历该矩形轮廓，排序子选项轮廓
                this->sortSubOptionContours(findNode.index);
                //拼接扫描结果：某个cell
                this->joinScanResultString(false, false, findNode.index, false);
            }
        }
        //拼接扫描结果：行结束
        this->joinScanResultString(false, true, -1, false);
    }
    //拼接扫描结果：补全结果
    this->joinScanResultString(false, false, -1, true);
}

/*子选项轮廓排序*/
void ZHCTableScaner::sortSubOptionContours(int contourIndex)
{
    vector<KDOptionNode> *optionContours = &(p_optionContourMap[contourIndex]);//存储子轮廓数量
    if ((*optionContours).size()<4) {
        //元素小于4个，认为是空轮廓
        return;
    }
    sort((*optionContours).begin(), (*optionContours).end(), zhc_compareOptionContour);
}

/*扫描结果拼接成二维数组字符串*/
void ZHCTableScaner::joinScanResultString(bool isRowHead, bool isRowTail, int contourIndex, bool isTableEnd)
{
    if (isRowHead) {
        //行头部
        this->p_scanResultStr+="[";//行开始，这里加一个开始符
    }
    else if (isRowTail) {
        //行尾部
        this->p_scanResultStr.pop_back();//移除最后一个分隔符逗号 ,
        this->p_scanResultStr+="],";//行结束，这里加一个结束符,并开启下一行
    } else if (isTableEnd) {
        //表格结束,补全分隔符号
        this->p_scanResultStr.insert(0,"[");//开头插入数组开始符号
        this->p_scanResultStr.pop_back();//拼接操作结束，移除最有一个分隔符逗号 ,
        this->p_scanResultStr+="]";//拼接操作结束，这里加一个结束符
    }
    if (contourIndex >=0){
        //不是行头，也不是行尾，拼接cell
        vector<KDOptionNode> optionContours = p_optionContourMap[contourIndex];
        if (optionContours.size()<=0) {
            return;
        }
        this->p_scanResultStr+='[';
        for (const auto &element : optionContours) {
#ifdef kZHCTableScaner_EnableLog
            p_loopCount ++;
#endif
            this->p_scanResultStr+=((element.isBlack)?"1":"0");
            this->p_scanResultStr+=",";
        }
        this->p_scanResultStr.pop_back();
        this->p_scanResultStr+="],";
    }
}

/*获取矩形轮廓的rect*/
cv::Rect ZHCTableScaner::getRectContourRect(int contourIndex)
{
    cv::Rect contourRect;
    map<int,Rect>::iterator rectIter = p_rectContourMap.find(contourIndex);
    if(rectIter != p_rectContourMap.end()) {
        //找到rect
        contourRect = rectIter->second;
    } else {
        //没有找到，生成一个rect
        vector<Point> superContour = p_contours[contourIndex];
        contourRect = boundingRect(superContour);
        p_rectContourMap[contourIndex] = contourRect;
    }
    return contourRect;
}

/*轮廓描边*/
void ZHCTableScaner::drawContours()
{
#ifdef kZHCTableScaner_EnableLog
    cout<<"[ZHCTableScaner]扫描结果:"<<p_scanResultStr<<endl;
    //矩形轮廓描边
    for (int i = 0; i < p_rectContours.size(); i++) {
        vector<Point> contourArea = p_rectContours[i];
        const Point *areaPoint = &contourArea[0];
        if (areaPoint->x > 3 && areaPoint->y > 3) {
            int n = (int)contourArea.size();
            polylines(p_originalMat, &areaPoint, &n, 1, true, Scalar(255, 0, 0), 1, LINE_AA);
        }
    }
    //矩形轮廓内添加文字
    for (int row = 0; row < p_areaData.size(); row++) {
        vector<KDRectNode > areaRow = p_areaData[row];
        for (int col = 0; col < areaRow.size(); col++) {
            KDRectNode areaNode = areaRow[col];
            string text = to_string(row)+"," + to_string(col);
            //将文本框居中绘制。默认其实位置文字左下角
            Point origin = Point(areaNode.x+5, areaNode.y+25);
            putText(p_originalMat, text, origin, FONT_HERSHEY_COMPLEX, 1, cv::Scalar(255, 0, 0,255), 1, FILLED, 0);

            //小轮廓内 标记是否涂黑
            map<int,vector<KDOptionNode>>::iterator nodeIter = p_optionContourMap.find(areaNode.index);
            if (nodeIter != p_optionContourMap.end()) {
                vector<KDOptionNode> optionNodes = nodeIter->second;
                for (int optionIndex = 0; optionIndex < optionNodes.size(); optionIndex++) {
                    KDOptionNode optionNode = optionNodes[optionIndex];
                    string text = to_string(optionIndex);//+","+to_string(optionNode.isBlack);
                    //将文本框居中绘制。默认其实位置文字左下角
                    Point origin = Point(optionNode.centerX-10, optionNode.centerY+15);
                    putText(p_originalMat, text, origin, FONT_HERSHEY_COMPLEX, 1, cv::Scalar(255, 0, 0,255), 1, LINE_AA, 0);
                }
            }
        }
    }
    p_resultMat = p_originalMat;
#endif
}

/*清除缓存、mat等等对象*/
void ZHCTableScaner::clean()
{
    p_areaList.clear();
    p_areaData.clear();
    p_contours.clear();
    p_rectContours.clear();
    p_optionContourMap.clear();
    p_rectContourMap.clear();
    p_resultMat = NULL;
    p_originalMat = NULL;
    p_resultCStr = NULL;
    p_scanResultStr.clear();
}


