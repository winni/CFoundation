//
// Created by 叶青 on 2017/3/2.
//  mat 掩码处理

#ifndef KDTEST_KDMATMASKOPERATE_H
#define KDTEST_KDMATMASKOPERATE_H

#include <string>
#include <opencv2/opencv.hpp>


class KDMatMaskOperate {

private:
    cv::Mat pri_mat;
public:
    KDMatMaskOperate();

    KDMatMaskOperate(std::string rPath);

    virtual ~KDMatMaskOperate();

   /*掩码操作*/
    void maskOperation();
};


#endif //KDTEST_KDMATMASKOPERATE_H
