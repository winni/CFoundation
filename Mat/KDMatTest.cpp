//
// Created by 叶青 on 2017/2/28.
//  Mat 常用方法


#include "KDMatTest.h"
#include <opencv2/highgui.hpp>
#include "../Utils/KDMatHelp.h"
#include "../Define/KDPrefixHeader.h"


using namespace std;
using namespace cv;


KDMatTest::KDMatTest() {

}

KDMatTest::KDMatTest(std::string rPath) {
    pri_mat = imread(rPath, CV_LOAD_IMAGE_COLOR );
    if(!pri_mat.data){
        KDLog("loadImg error,img is null");
        return ;
    }
//    cout << pri_mat << endl;
}

KDMatTest::~KDMatTest() {
    pri_mat.release();
    KDDealloc();
}

void KDMatTest::loadImg(std::string rPath) {
    Mat img = imread(rPath, 1 );
    if(!img.data){
        printf("loadImg error,img is null\n" );
        return ;
    }
    namedWindow("OpenCV Demo", WINDOW_AUTOSIZE );
    imshow("OpenCV Demo", img );
    waitKey();
}

void KDMatTest::show() {
    KDMatShowImg(pri_mat);
}

void KDMatTest::cut() {
    if(!pri_mat.data){
        printf("cut error,img is null\n" );
        return ;
    }
//    pri_mat = Mat(pri_mat, Rect(10, 10, 80, 80)); // using a rectangle
    pri_mat = Mat(pri_mat,Range::all(), Range(0,80));
    KDMatShowImg(pri_mat);
}

void KDMatTest::shallowCopy() {
    printf("mat data:%p,%d\n",pri_mat.data,pri_mat.rows);
    printf("mat_copy data:%p,%d\n",pri_mat_copy.data,pri_mat_copy.rows);
    //赋值、初始化都是浅拷贝
    pri_mat_copy = Mat(pri_mat);
//    pri_mat_copy = pri_mat;
    printf("mat data:%p,%d\n",pri_mat.data,pri_mat.rows);
    printf("mat_copy data:%p,%d\n",pri_mat_copy.data,pri_mat_copy.rows);
}

void KDMatTest::deepCopy() {
    printf("mat data:%p,%d\n",pri_mat.data,pri_mat.rows);
    printf("mat_copy data:%p,%d\n",pri_mat_copy.data,pri_mat_copy.rows);
    //赋值、初始化都是浅拷贝
    pri_mat_copy = pri_mat.clone();
    pri_mat.copyTo(pri_mat_copy);
    printf("mat data:%p,%d\n",pri_mat.data,pri_mat.rows);
    printf("mat_copy data:%p,%d\n",pri_mat_copy.data,pri_mat_copy.rows);

    printf("mat release\n");

    pri_mat_copy.release();

    printf("mat data:%p,%d\n",pri_mat.data,pri_mat.rows);
    printf("mat_copy data:%p,%d\n",pri_mat_copy.data,pri_mat_copy.rows);
}

void KDMatTest::release() {
    //先浅拷贝,retain count +1;
    pri_mat_copy = Mat(pri_mat);

    printf("mat data:%p,%d\n",pri_mat.data,pri_mat.rows);
    printf("mat_copy data:%p,%d\n",pri_mat_copy.data,pri_mat_copy.rows);

    //deallocates the data when reference counter reaches 0.引用计数=0,释放data
    pri_mat_copy.release();

    printf("mat data:%p,%d\n",pri_mat.data,pri_mat.rows);
    printf("mat_copy data:%p,%d\n",pri_mat_copy.data,pri_mat_copy.rows);

    KDMatShowImg(pri_mat);
}

void KDMatTest::showNewMat() {
    //1. 创建 100行,100列 三通道
//    Mat matTmp = Mat(100,100,CV_8UC3,Scalar(0,255,255));

    //2. 等价 1
//    int sz[2] = {100,100};
//    Mat matTmp = Mat(2,sz,CV_8UC3,Scalar(0,255,255));

    //像素随机
    int sz[2] = {100,100};
    Mat matTmp = Mat(2,sz,CV_8UC3);
    randu(matTmp,Scalar::all(0), Scalar::all(255));


    //3.三维图像,不能直接显示和输出
//    int sz[3] = {10,10,10};
//    Mat matTmp = Mat(3,sz,CV_32F);
////    Mat matTmp = Mat(3,sz,CV_8UC(1),Scalar::all(0));
//    randu(matTmp,Scalar::all(0), Scalar::all(255));

//    cout << "mat:" << matTmp << endl;
    KDMatShowImg(matTmp);
}