//
// Created by 叶青 on 2017/3/6.
//

#include "KDMatDraw.h"
#include "../Utils/KDMatHelp.h"
#include "../Define/KDPrefixHeader.h"

using namespace cv;

KDMatDraw::KDMatDraw() {}

KDMatDraw::~KDMatDraw() {
    KDDealloc();
}

void KDMatDraw::draw() {
    Mat atom_image = Mat::zeros( w, w, CV_8UC3 );
    Mat rook_image = Mat::zeros( w, w, CV_8UC3 );
}
