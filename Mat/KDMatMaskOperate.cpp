//
// Created by 叶青 on 2017/3/2.
//  mat 掩码处理

#include "KDMatMaskOperate.h"
#include <opencv2/highgui.hpp>
#include "../Utils/KDMatHelp.h"
#include "../Define/KDPrefixHeader.h"

using namespace std;
using namespace cv;

KDMatMaskOperate::KDMatMaskOperate() {}

KDMatMaskOperate::KDMatMaskOperate(std::string rPath) {
    pri_mat = imread(rPath, CV_LOAD_IMAGE_COLOR );
    if(!pri_mat.data){
        KDLog("loadImg error,img is null" );
        return ;
    }
}

KDMatMaskOperate::~KDMatMaskOperate() {
    pri_mat.release();
    KDDealloc();
}

void KDMatMaskOperate::maskOperation() {
    CV_Assert(pri_mat.depth() == CV_8U);  // accept only uchar images
    Mat matMask = Mat(pri_mat.size(),pri_mat.type());

    double beginTime= KDMatBeginTime();
    //1.滤波器
//    Mat matKern;
//    matKern = (Mat_<char>(3, 3) << 0, -1,  0,-1,  5, -1, 0, -1,  0);
//    filter2D(pri_mat,matMask,pri_mat.depth(),matKern);
    //2.遍历法
    const int matChannel = pri_mat.channels();
    for (int i = 1; i < pri_mat.rows-1; ++i) {
        const uchar *previousRow = pri_mat.ptr<uchar>(i-1);
        const uchar *currentRow = pri_mat.ptr<uchar>(i);
        const uchar *nextRow = pri_mat.ptr<uchar>(i+1);

        uchar* rowMask = matMask.ptr<uchar>(i);
        for (int j = matChannel; j < matChannel*(pri_mat.cols-1); ++j) {
            *rowMask = saturate_cast<uchar>(5*currentRow[j] - currentRow[j-matChannel] - currentRow[j+matChannel] - previousRow[j] - nextRow[j]);
            rowMask++;
        }
    }
    //设置边界
    pri_mat.row(0).copyTo(matMask.row(0));//第一行
    pri_mat.row(pri_mat.rows-1).copyTo(matMask.row(pri_mat.rows-1));//最后一行
    pri_mat.colRange(Range(1,matChannel)).copyTo(matMask.colRange(Range(1,matChannel)));//第一列(根据通道数目不一样)
    Range colLast = Range(pri_mat.cols-matChannel,pri_mat.cols);
    pri_mat.colRange(colLast).copyTo(matMask.colRange(colLast));//第一列(根据通道数目不一样)

    KDMatPrintTime(beginTime);
    KDMatShowImgs(&pri_mat,&matMask,NULL);
}



