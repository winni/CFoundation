//
//  MDScaleScaner.cpp
//  ScaleScaner
//
//  Created by YeQing on 2018/12/1.
//  Copyright ? 2018年 zhihan. All rights reserved.
//  表格扫描，c++实现

#include "MDScaleScaner.h"

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdocumentation"
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/imgcodecs/imgcodecs.hpp>
#pragma clang diagnostic pop

#include <iostream>
//#include "../Utils/KDMatHelp.h"

#define kMDScaleContourAreaMaxScale     1.08//轮廓和轮廓矩形 最大面积比例

#define kMDScaleNameMinScale        0.003//最小 name（矩形）面积
#define kMDScaleNameMaxScale        0.012//最大 name（矩形）面积
#define kMDScaleNameCropScale       0.35 // name（矩形）裁剪比例
#define kMDScaleNameSizeMinScale    2.5// name（矩形）最小宽高比

#define kMDScaleSectionAreaMinScale     0.16//最小section（矩形组）面积/大轮廓
#define kMDScaleSectionAreaMaxScale     0.31//最大section（矩形组）面积/大轮廓

#define kMDScaleCellAreaMinScale        0.002//最小cell（矩形）面积
#define kMDScaleCellAreaMaxScale        0.006//最大cell（矩形）面积
#define kMDScaleCellAreaBlurredScale    0.4//cell 模糊比例，即二值化后白色像素点比例

#define kMDScaleOptionAreaMinScale      0.00004 //最小option 面积
#define kMDScaleOptionAreaMaxScale      0.00020//最大option 面积

#define kMDScaleSectionCount            4//矩形组数量
#define kMDScaleSectionRowCount         5//一个矩形组内 行数量
#define kMDScaleSectionColumnCount       8//一个矩形组内 列数量
#define kMDScaleCellOneRowXDiff         2.0//同一行内,两个矩形 x差值（存在倾斜，且中间有间隔，预留200%）
#define kMDScaleCellOneRowYDiff         0.4//同一行内,两个矩形 y差值（存在倾斜，预留30%）
#define kMDScaleCellOneColumnYDiff      1.3//同一列内,两个矩形 x差值（存在倾斜，预留130%）
#define kMDScaleCellOneColumnXDiff      0.3//同一列内,两个矩形 y差值（存在倾斜，预留30%）

#define kMDScaleOptionSelectScale       0.8//选项区域 选中 比例
#define kMDScaleOptionSelectScale1      1.6//选项区域 选中和 未选中 两个区域的黑色像素比，如果比例偏差过大，认为有一个选中

#define kMDScaleROIOptionRectPaddingX      0.01//选项矩形内部padding，上下左右各排除10%
#define kMDScaleROIOptionExsitScale        0.04//选项区域 存在的比例， 黑色像素点超过总量的0.075，认为包含选项框,这个值和 kMDScaleROIOptionRectPaddingX 有关系
#define kMDScaleROIOptionSelectScale       0.19//选项区域 选中的比例， 黑色像素点超过总量的0.5，认为选中了选项框


using namespace cv;
using namespace std;
using namespace ZHScaner;

#pragma mark - Class MDScaleScaner
MDScaleScaner::MDScaleScaner() {
    this->p_loopCount = 0;
    this->p_originMat = NULL;
    this->enableDebug = false;
    this->questionCount = 0;
    //选项区域 选中 比例
    this->optionSelectedScale = kMDScaleOptionSelectScale;
    this->minMatSize = 0;
}

/** 析构函数 */
MDScaleScaner::~MDScaleScaner() {
    cout<<"[MDScaleScaner]~"<<endl;
}

/** 根据图片路径,开始扫描 */
bool MDScaleScaner::startScan(std::string rPath) {
    Mat fileMat = imread(rPath, IMREAD_COLOR);
    return this->startScan(fileMat);
}

/** 从根据mat对象,开始扫描 */
bool MDScaleScaner::startScan(cv::Mat inputMat) {
    if (this->enableDebug) {
        cout<<"[MDScaleScaner] start scan..."<<endl;
    }
    if (inputMat.data == NULL) {
        cout<<"[MDScaleScaner] scan error,input mat invalid"<<endl;
        return false;
    }
    this->clean();
    long long begintime = 0;
    if (this->enableDebug) {
        begintime = clock();//记录开始时间
    }
    p_originMat = inputMat;
    //中值滤波
    Mat tmpMat;
    medianBlur(inputMat, tmpMat, 1);
    resultMat = tmpMat;
    //灰度图像
    cvtColor(resultMat, tmpMat, COLOR_BGR2GRAY);
    resultMat = tmpMat;
    
//    //边缘检测
//    Laplacian(resultMat, tmpMat, CV_64F, 3, 3, 0, BORDER_DEFAULT);
//    Canny(resultMat, tmpMat, 20, 60, 3);
    
    //自适应二值化,THRESH_BINARY_INV: (src(x,y)>thresh)?0:maxval
    Mat thresholdMat,outerContourMat;
    bool isValidMat = false;
    for (int thresh = 67; thresh>=7; thresh-=2) {
        if (this->enableDebug) {
            cout<<""<<endl;
            cout<<"[MDScaleScaner] start threshold:"<<thresh<<endl;
        }
        if (resultMat.data == NULL) {
            return false;
        }
        adaptiveThreshold(resultMat, thresholdMat, 255, ADAPTIVE_THRESH_GAUSSIAN_C, THRESH_BINARY_INV, thresh, 5);
        //        threshold(resultMat, thresholdMat, thresh, 255, THRESH_BINARY_INV);
        //查找最大外轮廓
        isValidMat = this->findOuterContour(thresholdMat, &outerContourMat);
        if (!isValidMat) {
            continue;
        }
//        KDMatShowImg(thresholdMat);
        //查找并缓存轮廓层级
        findContours(outerContourMat, this->p_contours, this->p_hierarchys, RETR_TREE, CHAIN_APPROX_NONE);
        //根据方向，旋转轮廓
        Mat roataMat;
        isValidMat = this->rotateOriginMat(outerContourMat, &roataMat);
        if (!isValidMat) {
            continue;
        }
        if (roataMat.data != NULL) {
            //需要旋转，且旋转成功，重新检查轮廓
            //查找最大外轮廓
            isValidMat = this->findOuterContour(roataMat, &outerContourMat);
            if (!isValidMat) {
                continue;
            }
            //再次 查找并缓存轮廓层级
            findContours(outerContourMat, this->p_contours, this->p_hierarchys, RETR_TREE, CHAIN_APPROX_NONE);
        }
        //处理最大外轮廓，矩形轮廓分组
        this->processOuterContour(outerContourMat);
        //验证外轮廓
        isValidMat = this->validOuterContour(outerContourMat);
        if (!isValidMat) {
            continue;
        }
        //section 排序
        isValidMat = this->sortSectionCells(outerContourMat);
        if (!isValidMat) {
            continue;
        }
        //处理cell，查找题目
        isValidMat = this->processCells(outerContourMat);
        if (!isValidMat) {
            continue;
        }
        //校验题目数量
        if (this->enableDebug) {
            cout<<"[MDScaleScaner] start valid question count:"<<this->p_questionCount<<","<<this->questionCount<<endl;
        }
        if (this->questionCount>0) {
            //设置了问题数量，去校验问题总数
            if (this->p_questionCount != this->questionCount) {
                continue;
            } else {
                //题目数量正确
                break;
            }
        } else {
            //没有设置题目数量
            break;
        }
    }
    //生成扫描结果
    if (isValidMat) {
        isValidMat = this->generateResult();
    }
    //判断结果是够成功
    if (!isValidMat) {
        if (this->enableDebug) {
            cout<<"[MDScaleScaner] image invalid"<<endl;
        }
        return false;
    }
    //计时结束,打印时间
    if (this->enableDebug) {
        long long endtime = clock();
        printf("[MDScaleScaner] scan done,sum loop:%lu,time:%fs\n", p_loopCount, (double)(endtime-begintime)/CLOCKS_PER_SEC);
    }
    return true;
}

/* 查找 最大外轮廓 */
bool MDScaleScaner::findOuterContour(cv::Mat inputImg, cv::Mat *outputImg) {
    if (this->enableDebug) {
        cout<<"[MDScaleScaner] start find outer contour..."<<endl;
    }
    this->cleanThresholdCache();
    //查找最大外轮廓
    vector<Vec4i> outerAreaHierarchy;
    std::vector<std::vector<cv::Point> > outerContours;
    findContours(inputImg, outerContours, outerAreaHierarchy, RETR_TREE, CHAIN_APPROX_SIMPLE);
    int outerContourCount  = (int)outerContours.size();
    //遍历所有的轮廓，查找矩形轮廓
    Mat maxContour;
    std::map<int,int> outerContoursMap;
    for( size_t i = 0; i < outerContourCount; i++ ) {
        if (this->enableDebug) {
            p_loopCount ++;
        }
        Mat contourMatTmp(outerContours[i]);
        vector<Point> area;
        approxPolyDP(contourMatTmp, area, 5, true);
        //检测边数
        if (area.size() <4) {
            continue;
        }
        //tree模式,index:后、前、子、父
        Vec4i hierarchy = outerAreaHierarchy[i];
        int fatherHierarchyIndex = hierarchy[3];
        //有父轮廓
        if (fatherHierarchyIndex>=0) {
            int weights = outerContoursMap[fatherHierarchyIndex]+1;
            outerContoursMap[fatherHierarchyIndex] = weights;
            if (weights<30) {
                //子轮廓数量<30，继续循环
                continue;
            }
            //取出 父轮廓（大轮廓）的父轮廓
            Vec4i farterHierarchy = outerAreaHierarchy[fatherHierarchyIndex];
            int grandFatherHierarchyIndex = farterHierarchy[3];
            if (grandFatherHierarchyIndex<0) {
                //没有父轮廓，继续循环
                continue;
            }
            Mat superContourMat(outerContours[grandFatherHierarchyIndex]);
            maxContour = superContourMat;
            break;
        }
    }
    if (maxContour.data == NULL) {
        return false;
    }
    //找到外轮廓
    vector<Point> maxContourPoints;
    approxPolyDP(maxContour, maxContourPoints, 5, true);
    Mat maxContourNew(maxContourPoints);
    //裁剪 mat
    cv::Rect outputRect = boundingRect(maxContourNew);
    Mat originOutMat = inputImg(outputRect);
    
    //外轮廓 面积
    double contourSize = fabs(contourArea(maxContourNew));
    if (outputRect.width*outputRect.height*1.0/contourSize>kMDScaleContourAreaMaxScale) {
        if (this->enableDebug) {
            cout<<"[MDScaleScaner] find outer contour error:"<<contourSize<<","<<outputRect.width*outputRect.height<<endl;
        }
        return false;
    }
    this->p_outerContourArea = contourSize;
    //外轮廓 rect
    *outputImg = originOutMat;
    this->p_outerContourRect = outputRect;
    return true;
}

/* 根据方向，旋转原始Mat */
bool MDScaleScaner::rotateOriginMat(cv::Mat inputImg, cv::Mat *outputImg) {
    //找到 名称 轮廓
    if (this->enableDebug) {
        cout<<"[MDScaleScaner] start rotate mat..."<<endl;
    }
    if (inputImg.data  == NULL) {
        return false;
    }
    //计算矩形面积 范围
    const double minNameArea = this->p_outerContourArea*kMDScaleNameMinScale;
    const double maxNameArea = this->p_outerContourArea*kMDScaleNameMaxScale;
    const int contourCount  = (int)this->p_contours.size();
    //遍历所有的轮廓，查找昵称轮廓
    Rect nameRect;
    for( int index = 0; index < contourCount; index++ ) {
        if (this->enableDebug) {
            p_loopCount ++;
        }
        vector<Point> currentContour = p_contours[index];
        Mat currentMat(currentContour);
        vector<Point> currentContourNew;
        approxPolyDP(currentMat, currentContourNew, arcLength(currentMat, true) * 0.02, true);
        if (currentContourNew.size() <4 ) {
            //边数目过滤
            continue;
        }
        double areaArea = fabs(contourArea(currentContourNew));
        if (areaArea>=minNameArea && areaArea<=maxNameArea) {
            Rect currentRect = boundingRect(currentContourNew);//取出轮廓的rect
            if (currentRect.width*1.0/currentRect.height> kMDScaleNameSizeMinScale) {
                //如果当前rect 宽度比高度还要多3倍，认为是 姓名 区域
                //保留较大的name 区域
                if (currentRect.width > nameRect.width) {
                    nameRect = currentRect;
                }
            } else if (currentRect.height*1.0/currentRect.width> kMDScaleNameSizeMinScale) {
                //如果当前rect 高度比高度还要多3倍，认为是 姓名 区域
                //保留较大的name 区域
                if (currentRect.width > nameRect.width) {
                    nameRect = currentRect;
                }
            }
        }
    }
    //名称未找到
    if (nameRect.width<=0 || nameRect.height<=0) {
        if (this->enableDebug) {
            cout<<"[MDScaleScaner] rotate error,name not found"<<endl;
        }
        return false;
    }
    // 根据姓名 rect 来判断 方向
    if (nameRect.x< this->p_outerContourRect.width/2.0 &&  nameRect.y < this->p_outerContourRect.height/2.0) {
        //朝上,不变
        this->p_direction = Top;
        outputImg = NULL;
        
    } else if (nameRect.x> this->p_outerContourRect.width/2.0 &&  nameRect.y > this->p_outerContourRect.height/2.0) {
        //朝下，向右 180
        this->p_direction = Bottom;
        Mat transMat;
        flip(inputImg, transMat,-1);
        *outputImg = transMat;
        //旋转原图
        Mat originMatNew;
        flip(this->p_originMat, originMatNew,-1);
        this->p_originMat = originMatNew;
        //旋转 二值化前的图
        Mat thresholdMatNew;
        flip(this->resultMat, thresholdMatNew,-1);
        this->resultMat = thresholdMatNew;
        
    } else if (nameRect.x< this->p_outerContourRect.width/2.0 &&  nameRect.y > this->p_outerContourRect.height/2.0) {
        //朝左,向右 90
        this->p_direction = Left;
        Mat transMat;
        transpose(inputImg, transMat);
        flip(transMat, transMat, 1);
        //旋转原图
        Mat originMatNew;
        transpose(this->p_originMat, originMatNew);
        flip(originMatNew, this->p_originMat, 1);
        //旋转 二值化前的图
        Mat thresholdMatNew;
        transpose(this->resultMat, thresholdMatNew);
        flip(thresholdMatNew, this->resultMat, 1);
        
    } else if (nameRect.x> this->p_outerContourRect.width/2.0 &&  nameRect.y < this->p_outerContourRect.height/2.0) {
        //朝右，向右 270
        this->p_direction = Right;
        Mat transMat;
        transpose(inputImg, transMat);
        flip(transMat, *outputImg, 0);
        //旋转原图
        Mat originMatNew;
        transpose(this->p_originMat, originMatNew);
        flip(originMatNew, this->p_originMat, 0);
        //旋转 二值化前的图
        Mat thresholdMatNew;
        transpose(this->resultMat, thresholdMatNew);
        flip(thresholdMatNew, this->resultMat, 0);
        
    } else {
        //其他case，朝上
        this->p_direction = Top;
        outputImg = NULL;
    }
    if (this->enableDebug) {
        cout<<"[MDScaleScaner] end rotate mat:"<<this->p_direction<<endl;
    }
    return true;
}

/* 处理 最大外轮廓,查找section、cell、option */
void MDScaleScaner::processOuterContour(cv::Mat inputImg) {
    if (this->enableDebug) {
        cout<<"[MDScaleScaner] start process outer contour..."<<endl;
    }
    if (inputImg.data == NULL) {
        return;
    }
    //计算矩形面积 范围
    const double minNameArea = p_outerContourArea*kMDScaleNameMinScale;
    const double maxNameArea = p_outerContourArea*kMDScaleNameMaxScale;
    const double minRectArea = p_outerContourArea*kMDScaleCellAreaMinScale;
    const double maxRectArea = p_outerContourArea*kMDScaleCellAreaMaxScale;
    const double minSectionArea = p_outerContourArea*kMDScaleSectionAreaMinScale;
    const double maxSectionArea = p_outerContourArea*kMDScaleSectionAreaMaxScale;
    const double minOptionArea = p_outerContourArea*kMDScaleOptionAreaMinScale;
    const double maxOptionArea = p_outerContourArea*kMDScaleOptionAreaMaxScale;
    //遍历所有的轮廓，查找矩形轮廓
    const int contourCount  = (int)p_contours.size();
    for( int index = 0; index < contourCount; index++ ) {
        if (this->enableDebug) {
            p_loopCount ++;
        }
        vector<Point> currentContour = p_contours[index];
        Mat currentMat(currentContour);
        vector<Point> currentContourNew;
        approxPolyDP(currentMat, currentContourNew, arcLength(currentMat, true) * 0.02, true);
        if (currentContourNew.size() <4 ) {
            //边数目过滤
            continue;
        }
        double areaArea = fabs(contourArea(currentContourNew));
        /////先判断是不是昵称 区域////////////////////
        if (areaArea>=minNameArea && areaArea<=maxNameArea) {
            Rect currentRect = boundingRect(currentContourNew);//取出轮廓的rect
            if (currentRect.width*1.0/currentRect.height> kMDScaleNameSizeMinScale) {
                //如果当前rect 宽度比高度还要多3倍，认为是 姓名 区域
                if (currentRect.y>currentRect.height*10.0) {
                    //如果 姓名区域 y值比 高度多10倍，认为 姓名区域可能是在底部，图片倒着的，不合法,退出此次查找
                    return ;
                }
                //保留较大的name 区域
                if (currentRect.width > p_nameMatRect.width) {
                    p_nameMatRect = currentRect;
                }
                continue;
            }
        }
        /////再检测 矩形轮廓面积////////////////////////
        if (areaArea>=minRectArea && areaArea<=maxRectArea) {
            //轮廓区域面积 符合要求
            Rect currentRect = boundingRect(currentContourNew);//取出轮廓的rect
            if (currentRect.height - currentRect.width>= currentRect.width*0.5) {
                //如果当前rect 高度比宽度还要多0.5的宽度，则不合法。
                continue;
            }
            if (currentRect.width*1.0/currentRect.height>= 3.0) {
                //如果当前rect 宽度比高度还要多3倍，则不合法。
                continue;
            }
            MDScaleCell rectangle;
            rectangle.index = index;
            rectangle.x = currentRect.x;
            rectangle.y = currentRect.y;
            rectangle.width = currentRect.width;
            rectangle.height = currentRect.height;
            rectangle.distance = sqrt(rectangle.x*rectangle.x+rectangle.y*rectangle.y);
            //找到中心点
            Moments areaM = moments(currentContour);
            float centerX = float(areaM.m10 / areaM.m00);
            float centerY = float(areaM.m01 / areaM.m00);
            rectangle.centerX = centerX;
            rectangle.centerY = centerY;
            //矩形区域 分组
            Vec4i currentHierarchy = p_hierarchys[index];
            int fatherHierarchyIndex = currentHierarchy[3];
            if (fatherHierarchyIndex>=0) {
                //找到父层级
                vector<MDScaleCell> *sectionRects = &(p_sectionMap[fatherHierarchyIndex]);
                //如果数组为空
                if (sectionRects->size()<=0) {
                    sectionRects->push_back(rectangle);
                    continue ;
                }
                //判断和第一个点的对角线距离，升序排列，这里不具体排序，只选取出最左上角的节点
                MDScaleCell firstArea = (*sectionRects)[0];
                if (rectangle.distance<=firstArea.distance) {
                    //要插入到节点 距离原点更近
                    sectionRects->insert(sectionRects->begin(), rectangle);
                } else {
                    sectionRects->push_back(rectangle);
                }
            } else {
                //找不到父层级，加入到队列中，下一步重新分组
                p_noSuperCells.push_back(rectangle);
            }
        }
        else if (areaArea>=minSectionArea && areaArea<=maxSectionArea) {
            //////矩形组 section//////////////////
            //轮廓矩形面积再次校验，有可能是弯弯的轮廓，轮廓面积符合条件，但是对应矩形面积不一定符合
            Rect sectionRect = boundingRect(currentContourNew);//取出轮廓的rect
            double sectionRectArea = sectionRect.width * sectionRect.height;
            if (sectionRectArea > maxSectionArea) {
                //轮廓矩形面积超出范围
                continue;
            }
            MDScaleSection section;
            section.index = index;
            section.x = sectionRect.x;
            section.y = sectionRect.y;
            section.width = sectionRect.width;
            section.height = sectionRect.height;
            section.distance = sqrt(section.x*section.x+section.y*section.y);
            //如果数组为空
            if (p_sections.size()<=0) {
                p_sections.push_back(section);
                continue ;
            }
            //判断和第一个点的对角线距离，升序排列，这里不具体排序，只选取出最左上角的节点
            MDScaleSection firstSection = p_sections[0];
            if (section.distance<=firstSection.distance) {
                //要插入到节点 距离原点更近
                p_sections.insert(p_sections.begin(), section);
            } else {
                p_sections.push_back(section);
            }
        }
        else if (areaArea >= minOptionArea && areaArea <= maxOptionArea) {
            //找到 选项轮廓
            Rect currentRect = boundingRect(currentContourNew);//取出轮廓的rect
            MDScaleOption option;
            option.index = index;
            option.x = currentRect.x;
            option.y = currentRect.y;
            option.width = currentRect.width;
            option.height = currentRect.height;
            Moments optionM = moments(currentContour);
            float centerX = float(optionM.m10 / optionM.m00);
            float centerY = float(optionM.m01 / optionM.m00);
            option.centerX = centerX;
            option.centerY = centerY;
            Vec4i currentHierarchy = p_hierarchys[index];
            int fatherHierarchyIndex = currentHierarchy[3];
            if (fatherHierarchyIndex>=0) {
                //找到父层级
                vector<MDScaleOption> *options = &(p_optionMap[fatherHierarchyIndex]);
                options->push_back(option);
            }
        }
        //        else {//if (areaArea<0.3*minRectArea) {
        //            //test
        //            Rect sectionRect = boundingRect(currentContourNew);//取出轮廓的rect
        //            cout<<minRectArea<<","<<maxRectArea<<","<<areaArea<<","<<areaArea*1.0/p_outerContourArea<<endl;
        //            KDMatShowImg("不合法的", inputImg(sectionRect));
        //        }
    }
}

/* 校验 最大外轮廓,校验section、cell 数量是否合法 */
bool MDScaleScaner::validOuterContour(cv::Mat inputImg) {
    if (this->enableDebug) {
        cout<<"[MDScaleScaner] start valid outer contour..."<<endl;
    }
    ///////校验 矩形section 数量//////////////
    const int sectionCount = (int)p_sections.size();
    if (sectionCount < kMDScaleSectionCount) {
        if (this->enableDebug) {
            cout<<"[MDScaleScaner] valid outer contour,error,section count:"<<sectionCount<<endl;
        }
        return false;
    }
    ///////重新校验 无父层级的轮廓//////////////
    int noSuperRectCount = (int)p_noSuperCells.size();
    if (noSuperRectCount>0) {
        if (this->enableDebug) {
            cout<<"[MDScaleScaner] valid outer contour,has no super rects:"<<noSuperRectCount<<endl;
        }
        for (int rectIndex = 0; rectIndex < noSuperRectCount; rectIndex++) {
            MDScaleCell rect = p_noSuperCells[rectIndex];
            //遍历 section ，重新分组
            for (int sectionIndex = 0; sectionIndex < sectionCount; sectionIndex++) {
                MDScaleSection section = p_sections[sectionIndex];
                if (rect.x>=section.x && rect.y<=section.y && rect.width<=section.width && rect.height<=section.height) {
                    //包含在某个section 内
                    vector<MDScaleCell> *sectionRects = &(p_sectionMap[section.index]);
                    if (sectionRects->size()<=0) {
                        //如果数组为空
                        sectionRects->push_back(rect);
                        continue ;
                    }
                    //判断和第一个点的对角线距离，升序排列，这里不具体排序，只选取出最左上角的节点
                    MDScaleCell firstArea = (*sectionRects)[0];
                    if (rect.distance<=firstArea.distance) {
                        //要插入到节点 距离原点更近
                        sectionRects->insert(sectionRects->begin(), rect);
                    } else {
                        sectionRects->push_back(rect);
                    }
                }
            }
        }
    }
    ///////校验 矩形cell 数量//////////////
    int validSectionCount = 0;
    //    int validCellCount = 0;
    map<int, vector<MDScaleCell>>::iterator iter = p_sectionMap.begin();
    while(iter != p_sectionMap.end())  {
        vector<MDScaleCell> cells = iter->second;
        int cellCount = (int)cells.size();
        if (cellCount >= kMDScaleSectionRowCount*kMDScaleSectionColumnCount) {
            //cell 个数满足
            validSectionCount ++;
            //////校验 option 个数////////
            //            for (int cellIndex = 0; cellIndex < cellCount; cellIndex++) {
            //                MDScaleCell cell = cells[cellIndex];
            //            }
        } else {
            //某个section 里的cell 个数不满足要求
            if (this->enableDebug) {
                cout<<"[MDScaleScaner] valid outer contour,warning,section:"<<iter->first<<" only has "<<cells.size()<<" rect"<<endl;
                p_loopCount ++;
            }
        }
        iter ++;
    }
    if (validSectionCount < kMDScaleSectionCount) {
        if (this->enableDebug) {
            cout<<"[MDScaleScaner] valid outer contour,error,section count:"<<validSectionCount<<endl;
        }
        return false;
    }
    
    return true;
}

/* 排序 section 和 cell */
bool MDScaleScaner::sortSectionCells(cv::Mat inputImg) {
    if (this->enableDebug) {
        cout<<"[MDScaleScaner] start sort sections..."<<endl;
    }
    const int sectionCount = (int)p_sections.size();
    for (int i = 1; i < sectionCount; i++) {
        MDScaleSection insertSection = p_sections[i];
        int j = i;
        //如果 待插入元素比 有序序列的元素小，把有序的元素往后移
        while (j-1>=0 && insertSection.distance <= p_sections[j-1].distance) {
            if (this->enableDebug) {
                p_loopCount ++;
            }
            p_sections[j] = p_sections[j-1];
            j--;
        }
        //找到要插入的位置，插入元素
        p_sections[j] = insertSection;
    }
    ///////排序 所有的矩形//////////
    for (int k = 0; k < sectionCount; k++) {
        MDScaleSection *section = &(p_sections[k]);
        int index = (*section).index;
        vector<vector<_MDScaleCell>> *cells = &(section->cells);
        if (cells== NULL) {
            continue;
        }
        //按组排序
        vector<MDScaleCell> *rects = &(p_sectionMap[index]);//一个section里的rects
        for (int row = 0; row < kMDScaleSectionRowCount; row++) {
            int rectIndex = -1;
            MDScaleCell headRect;
            if (row == 0) {
                //第一行
                rectIndex = 0;
                headRect = (*rects)[0];
                if (this->enableDebug) {
                    p_loopCount ++;
                }
            } else {
                //其他行
                MDScaleCell lastRow = (*cells)[cells->size()-1][0];//上一行第一列
                float minDiffY = lastRow.height*kMDScaleCellOneColumnYDiff;//两个点Y距离,这里预留30%的像素的差值，理论上，每两行间距=行高
                const float minDiffX = lastRow.width*kMDScaleCellOneColumnXDiff;//同一列，上下两个rect x偏移预留30%
                //遍历section 剩余的 rects
                for (int i = 0; i < rects->size(); i++) {
                    if (this->enableDebug) {
                        p_loopCount++;
                    }
                    MDScaleCell rectTmp = (*rects)[i];
                    float diffX = rectTmp.x - lastRow.x;
                    float diffY = rectTmp.y - lastRow.y;
                    if (fabs(diffX) <= minDiffX && diffY >= 0 && diffY <= minDiffY) {
                        //找 x差值在20以内，y距离最小的
                        minDiffY = diffY;
                        rectIndex = i;
                        headRect = rectTmp;
                    }
                }
            }
            if (rectIndex<0) {
                //找不到行头，不继续找后面的列，也不继续找下面的行
                if (this->enableDebug) {
                    cout<<"[MDScaleScaner] sort sections,error,section head not found,section:"<<k<<",row:"<<row<<endl;
                }
                return false;
            }
            //找到行 头部列,新增一行 矩形
            vector<MDScaleCell > rectRowNew;
            rectRowNew.push_back(headRect);
            cells->push_back(rectRowNew);
            rects->erase(rects->begin()+rectIndex);//删除已经找到的节点
            
            ///////排序 后面的列//////////
            vector<MDScaleCell > *rectRow = &((*cells)[row]);
            for (int col = 1; col < kMDScaleSectionColumnCount; col++) {
                int rectSize = (int)rectRow->size();
                MDScaleCell lastColumn = (*rectRow)[rectSize-1];
                float minDiffX = lastColumn.width*kMDScaleCellOneRowXDiff;//同一行内的rect x差值
                const float maxDiffY = lastColumn.height*kMDScaleCellOneRowYDiff;//同一行内的rect y差值
                
                int nextRectIndex = -1;
                MDScaleCell nextRect;
                //遍历section 剩余的 rects
                for (int i = 0; i < rects->size(); ++i) {
                    if (this->enableDebug) {
                        p_loopCount ++;
                    }
                    MDScaleCell rectTmp = (*rects)[i];
                    float diffX = rectTmp.x-lastColumn.x;
                    float diffY = rectTmp.y-lastColumn.y;
                    if (diffX<0 || diffX>minDiffX) {
                        continue;
                    }
                    if (fabs(diffY)<=maxDiffY) {
                        //找 y差值在阈值以内，x差距最小的
                        minDiffX = diffX;
                        nextRectIndex = i;
                        nextRect = rectTmp;
                    }
                }
                if (nextRectIndex>=0) {
                    //找到后面的列，存储
                    rectRow->push_back(nextRect);
                    rects->erase(rects->begin()+nextRectIndex);//删除已经找到的节点
                } else {
                    if (this->enableDebug) {
                        cout<<"[MDScaleScaner] sort sections,error,column not found,section:"<<k<<",row:"<<row<<",col:"<<col<<endl;
                    }
                    return false;
                }
            }
        }
        /////剔除section所有行的第一列，合并到新的vetor里////////////////////
        for (int row = 0; row < (*cells).size(); ++row) {
            vector<MDScaleCell> *rectRow = &((*cells)[row]);
            rectRow->erase(rectRow->begin());
            //合并到新的vector里
            vector<MDScaleCell> *resultRow = &(p_cells[row]);
            resultRow->insert(resultRow->end(),rectRow->begin(),rectRow->end());
            if (this->enableDebug) {
                p_loopCount ++;
            }
        }
    }
    //清除 sections 缓存
    p_sections.clear();
    p_sectionMap.clear();
    return true;
}

/* 处理 cell,选取 所有的题目 */
bool MDScaleScaner::processCells(cv::Mat inputImg) {
    if (this->enableDebug) {
        cout<<"[MDScaleScaner] start process cells..."<<endl;
    }
    //倒序处理，一般拍摄的阴影都在底部
    for (int rowIndex = ((int)this->p_cells.size())-1; rowIndex >=0 ; rowIndex--) {
        int columnCount = ((int)this->p_cells[rowIndex].size());
        for (int columnIndex = columnCount-1; columnIndex >=0; columnIndex--) {
            MDScaleCell *cell = &(p_cells[rowIndex][columnIndex]);
            if (this->enableDebug) {
                cell->row = rowIndex;
                cell->column = columnIndex;
            }
            
            //找题目1
            int questionCountTmp = 0;//默认没有题目
            bool questionAValid = true;//是否有效
            MDScaleQuestion *questionA = this->processQuestion(inputImg, cell, 0, &questionAValid);
            if (!questionAValid) {
                if (this->enableDebug) {
                    cout<<"[MDScaleScaner] process cells error,quesiton1 invalid:"<<rowIndex<<","<<columnIndex<<endl;
                }
                return false;
            }
            if (questionA == NULL) {
                //第一题没找到，继续找下一个cell
                if (this->enableDebug) {
                    cout<<"[MDScaleScaner] process cells,quesiton1 not found:"<<rowIndex<<","<<columnIndex<<endl;
                }
                continue;
            }
            questionCountTmp ++;
            this->p_questionCount ++;
            //找题目2
            bool questionBValid = true;//是否有效
            MDScaleQuestion *questionB = this->processQuestion(inputImg, cell, 1, &questionBValid);
            if (!questionAValid) {
                if (this->enableDebug) {
                    cout<<"[MDScaleScaner] process cells error,quesiton2 invalid:"<<rowIndex<<","<<columnIndex<<endl;
                }
                return false;
            }
            if (questionB == NULL) {
                if (this->enableDebug) {
                    cout<<"[MDScaleScaner] process cells warning,quesiton2 not found:"<<rowIndex<<","<<columnIndex<<endl;
                }
            } else {
                questionCountTmp ++;
                this->p_questionCount ++;
            }
            if (questionCountTmp>0 && questionA!= NULL) {
                cell->questionCount = questionCountTmp;
                cell->questions = (MDScaleQuestion *) malloc(sizeof(MDScaleQuestion) * (cell->questionCount));
                cell->questions[0] = *questionA;
                free(questionA);
                questionA = NULL;
                if (questionB != NULL) {
                    cell->questions[1] = *questionB;
                    free(questionB);
                    questionB = NULL;
                }
            }
        }
    }
    return true;
}

/* 处理 单个题目以及 选择的选项值 */
MDScaleQuestion* MDScaleScaner::processQuestion(cv::Mat inputImg, MDScaleCell *cell, int questionIndex, bool *valid) {
    vector<MDScaleOption> *options = &(this->p_optionMap[cell->index]);
    int optionCount = (int)options->size();
    if (optionCount <= 0) {
        return NULL;
    }
    bool optionAUseROI = false;//选项A 是否使用roi的方式去检测选项
    bool optionBUseROI = false;//选项A 是否使用roi的方式去检测选项
    Rect optionRect1, optionRect2;
    MDScaleQuestion *questionResult = NULL;
    /////构造 选项区域 rect
    if (questionIndex == 0) {
        //第一行 选项A
        const float firstOption1Width = (cell->centerX - cell->x);
        const float firstOption1Height = (cell->centerY - cell->y);
        optionRect1.x = cell->x;
        optionRect1.y = cell->y;
        optionRect1.width = firstOption1Width;
        optionRect1.height = firstOption1Height;
        //第一行 选项B
        const float firstOption2Width = cell->width - (cell->centerX - cell->x);
        //        const float firstOption2Height = firstOption1Height;
        optionRect2.x = cell->centerX;
        optionRect2.y = cell->y;
        optionRect2.width = firstOption2Width;
        optionRect2.height = optionRect1.height;
    } else if(questionIndex == 1) {
        //第二行 选项A
        const float secondOption1Width = cell->centerX - cell->x;
        const float secondOption1Height = cell->height - (cell->centerY - cell->y);
        optionRect1.x = cell->x;
        optionRect1.y = cell->centerY;
        optionRect1.width = secondOption1Width;
        optionRect1.height = secondOption1Height;
        //第二行 选项B
        const float secondOption2Width = cell->width - (cell->centerX - cell->x);
        //        const float secondOption2Height = secondOption1Height;
        optionRect2.x = cell->centerX;
        optionRect2.y = cell->centerY;
        optionRect2.width = secondOption2Width;
        optionRect2.height = optionRect1.height;
    }
    //选项 位置过滤
    if (optionRect1.width ==0 || optionRect1.height == 0 || optionRect2.width ==0 || optionRect2.height == 0) {
        return NULL;
    }
    //遍历选项列表，把选项放在正确的位置上
    MDScaleOption optionA, optionB;
    for (vector<MDScaleOption>::iterator it = options->begin(); it != options->end();  ) {
        MDScaleOption option = *it;
        if (option.centerX>optionRect1.x && option.centerY>optionRect1.y && option.centerX<(optionRect1.x+optionRect1.width) && option.centerY<(optionRect1.y+optionRect1.height)) {
            //选项1
            optionA.index = option.index;
            optionA.x = option.x;
            optionA.y = option.y;
            optionA.width = option.width;
            optionA.height = option.height;
            optionA.centerX = option.centerX;
            optionA.centerY = option.centerY;
            options->erase(it);
            //如果选项B 已经找到 break;
            if (optionB.width>0 && optionB.height>0) {
                break;
            }
            continue;
        }
        if (option.centerX>optionRect2.x && option.centerY>optionRect2.y && option.centerX<(optionRect2.x+optionRect2.width) && option.centerY<(optionRect2.y+optionRect2.height)) {
            //选项2
            optionB.index = option.index;
            optionB.x = option.x;
            optionB.y = option.y;
            optionB.width = option.width;
            optionB.height = option.height;
            optionB.centerX = option.centerX;
            optionB.centerY = option.centerY;
            options->erase(it);
            //如果选项A 已经找到 break;
            if (optionA.width > 0 && optionA.height > 0) {
                break;
            }
            continue;
        }
        it++;
    }
    //校验选项合法性
    if (optionA.width<=0 || optionA.height<=0) {
        //选项A 不存在，采用ROI 方式继续查找(可能涂的时候，涂到cell 边框上了，这样会识别不出来)
        optionAUseROI = true;
    }
    if (optionB.width<=0 || optionB.height<=0) {
        //选项B 不存在，采用ROI 方式继续查找(可能涂的时候，涂到cell 边框上了，这样会识别不出来)
        optionBUseROI = true;
    }
    //检测选项是否被存在、是否被选中
    bool optionAExsit = false;//选项A 是否存在
    bool optionASelected = false;//选项A 是否选中
    float optionAScale = 0;//选项A 黑色点的比例
    bool optionBExsit = false;//选项B 是否存在
    bool optionBSelected = false;//选项B 是否选中
    float optionBScale = 0;//选项B 黑色点的比例
    if (optionAUseROI && optionBUseROI) {
        //两题的轮廓都没有，认为没有这道题目
        return  NULL;
        //        //使用ROI方式,查找A、B
        //        this->processOptionByROI(inputImg, optionA, &optionAExsit, &optionASelected, &optionAScale, *cell);
        //        this->processOptionByROI(inputImg, optionB, &optionBExsit, &optionASelected, &optionAScale, *cell);
        //        //如果都选中了，使用涂黑面积大的
        //        if (optionASelected && optionBSelected) {
        //            optionASelected = (optionAScale >= optionBScale) ? true : false;
        //        }
    } else if (optionAUseROI && !optionBUseROI) {
        //A选项没找到，先判断是否是cell过于模糊导致的，还是涂到边框上去了
        bool blurred = this->checkCellBlurred(inputImg, cell);
        if (blurred) {
            *valid = false;
            return NULL;
        }
        //使用ROI方式查找A，使用轮廓方式查找B
        optionAExsit = true;
        optionA.x = optionRect1.x;
        optionA.y = optionRect1.y;
        optionA.width = optionRect1.width;
        optionA.height = optionRect1.height;
        this->processOptionByROI(inputImg, optionA, &optionAExsit, &optionASelected, &optionAScale, *cell);
        optionBExsit = true;
        this->processOptionByContour(inputImg, optionB, &optionBSelected, &optionBScale);
        if (optionBSelected && optionASelected) {
            //优先使用 轮廓选中的结果
            optionASelected = false;
        }
    } else if (!optionAUseROI && optionBUseROI) {
        //B选项没找到，先判断是否是cell过于模糊导致的，还是涂到边框上去了
        bool blurred = this->checkCellBlurred(inputImg, cell);
        if (blurred) {
            *valid = false;
            return NULL;
        }
        //使用ROI方式查找B，使用轮廓方式查找A
        optionBExsit = true;
        optionB.x = optionRect2.x;
        optionB.y = optionRect2.y;
        optionB.width = optionRect2.width;
        optionB.height = optionRect2.height;
        this->processOptionByROI(inputImg, optionB, &optionBExsit, &optionBSelected, &optionBScale, *cell);
        optionAExsit = true;
        this->processOptionByContour(inputImg, optionA, &optionASelected, &optionAScale);
        if (optionBSelected && optionASelected) {
            //优先使用 轮廓选中的结果
            optionBSelected = false;
        }
    } else {
        //使用轮廓的方式查找 A、B
        optionAExsit = true;
        optionBExsit = true;
        this->processOptionByContour(inputImg, optionA, &optionASelected, &optionAScale);
        this->processOptionByContour(inputImg, optionB, &optionBSelected, &optionBScale);
        //如果都选中了，使用涂黑面积大的
        if (optionASelected && optionBSelected) {
            optionASelected = (optionAScale>=optionBScale)? true: false;
        }
    }
    if (this->enableDebug) {
        cout<<"[MDScaleScaner] processQuestion,option scale:"<<cell->row<<","<<cell->column<<","<<optionAUseROI<<","<<optionBUseROI<<","<<optionAScale<<","<<optionBScale<<endl;
    }
    if (!optionAExsit || !optionBExsit) {
        if (this->enableDebug) {
            cout<<"[MDScaleScaner] processQuestion,option not exsit:"<<cell->row<<","<<cell->column<<","<<optionAUseROI<<","<<optionBUseROI<<","<<optionAExsit<<","<<optionBExsit<<endl;
        }
        return NULL;
    }
    //构造 question
    MDScaleQuestion *question = (MDScaleQuestion *) malloc(sizeof(MDScaleQuestion));
    question->optionA.x = optionRect1.x;
    question->optionA.y = optionRect1.y;
    question->optionA.width = optionRect1.width;
    question->optionA.height = optionRect1.height;
    question->optionB.x = optionRect2.x;
    question->optionB.y = optionRect2.y;
    question->optionB.width = optionRect2.width;
    question->optionB.height = optionRect2.height;
    if (optionASelected) {
        question->value = 1;
    } else if (optionBSelected) {
        question->value = 0;
    } else {
        question->value = -1;
    }
    questionResult = question;
    return questionResult;
}

/* 检测 cell 是否模糊（只要当某个问题 某个选项找不到的时候，才去检测是否模糊） */
bool MDScaleScaner::checkCellBlurred(cv::Mat inputImg, ZHScaner::MDScaleCell *cell) {
    if (cell->blurred<0) {
        //cell 之前没有检测过模糊值
        vector<Point> cellPoints = this->p_contours[cell->index];
        long kContourPoints = 0;//选项轮廓 点的数量
        long validPointCount = 0;//实际黑色点数
        for (int row = 0; row < cell->height; row++) {
            for (int col = 0; col < cell->width; col++) {
                if (this->enableDebug) {
                    p_loopCount++;
                }
                //判断是否在轮廓内
                //（返回值为+1，表示点在多边形内部，返回值为-1，表示在多边形外部，返回值为0，表示在多边形上）
                int contain = pointPolygonTest(cellPoints, Point2f(cell->x + col, cell->y + row), false);
                if (contain < 0) {
                    //在轮廓外部
                    continue;
                }
                kContourPoints++;
                uchar pointColor = inputImg.at<uchar>(cell->y + row, cell->x + col);
                if (pointColor >= 254) {//二值化后，涂黑的变成白色，这里判断255,255,255
                    validPointCount++;
                }
            }
        }
        float blurredScale = validPointCount * 1.0 / kContourPoints;
        if (this->enableDebug) {
            cout << "[MDScaleScaner] check cell blurred:" << cell->row << "," << cell->column << "," << blurredScale
            << endl;
        }
        if (blurredScale > kMDScaleCellAreaBlurredScale) {
            //模糊
            cell->blurred = 1;
        } else {
            //不模糊
            cell->blurred = 0;
        }
    }
    if (cell->blurred == 1) {
        return  true;
    } else {
        return false;
    }
}

/* 通过轮廓的方式处理单个选项，判断是否选中 */
void MDScaleScaner::processOptionByContour(cv::Mat inputImg, MDScaleOption option, bool *selected, float *optionScale) {
    *selected = false;//默认未选中
    vector<Point> optionContour = this->p_contours[option.index];
    long kContourPoints = 0;//选项轮廓 点的数量
    long validPointCount = 0;//实际黑色点数
    for (int row = 0; row < option.height; row++) {
        for (int col = 0; col < option.width; col++) {
            if (this->enableDebug) {
                p_loopCount ++;
            }
            //判断是否在轮廓内
            //（返回值为+1，表示点在多边形内部，返回值为-1，表示在多边形外部，返回值为0，表示在多边形上）
            int contain = pointPolygonTest(optionContour, Point2f(option.x+col, option.y+row), false);
            if (contain<0) {
                //在轮廓外部
                continue;
            }
            kContourPoints++;
            uchar pointColor = inputImg.at<uchar>(option.y+row, option.x+col);
            if (pointColor >=254) {//二值化后，涂黑的变成白色，这里判断255,255,255
                validPointCount ++;
            }
        }
    }
    *optionScale = validPointCount*1.0/kContourPoints;
    //有效黑色点数超过  选中的阈值
    if (*optionScale>=this->optionSelectedScale) {
        *selected = true;
    }
}

/* 通过ROI的方式处理 单个选项，判断是否存在，以及是否选中 */
void MDScaleScaner::processOptionByROI(cv::Mat inputImg, MDScaleOption option, bool *exsit, bool *selected, float *optionScale, MDScaleCell cell) {
    *exsit = false;
    *selected = false;//默认未选中
    if (option.width<=0 || option.height<=0) {
        return;
    }
    vector<Point> cellContour = this->p_contours[cell.index];
    const int rowOrigin = option.height*kMDScaleROIOptionRectPaddingX;//行起点
    const int columnOrigin = option.width*kMDScaleROIOptionRectPaddingX;//列起点
    const int rowCount = option.height*(1-2*kMDScaleROIOptionRectPaddingX);//实际行
    const int colCount = option.width*(1-2*kMDScaleROIOptionRectPaddingX);//实际列
    const long kExsitMinPointCount = rowCount*colCount*kMDScaleROIOptionExsitScale;//选项区域存在，最少的黑色点数
    const long kSelectMinPointCount = rowCount*colCount*kMDScaleROIOptionSelectScale;//选项区域被选中，最少的黑色点数
    long validPointCount = 0;//实际黑色点数
    for (int row = rowOrigin; row < rowCount; row++) {
        for (int col = columnOrigin; col < colCount; col++) {
            if (this->enableDebug) {
                p_loopCount ++;
            }
            uchar pointColor = inputImg.at<uchar>(option.y+row, option.x+col);
            if (pointColor >=254) {//二值化后，涂黑的变成白色，这里判断255,255,255
                validPointCount ++;
            }
            //获取临时的涂黑比例
            if (validPointCount>kSelectMinPointCount) {
                break;
            }
        }
        if (validPointCount>kSelectMinPointCount) {
            break;
        }
    }
    *optionScale = validPointCount*1.0/(rowCount*colCount);
    //有效黑色点数超过  存在的阈值
    if (validPointCount>=kExsitMinPointCount) {
        *exsit = true;
    }
    //有效黑色点数超过  选中的阈值
    if (validPointCount>=kSelectMinPointCount) {
        *selected = true;
    }
}

/* 生成扫描结果 */
bool MDScaleScaner::generateResult() {
    if (this->enableDebug) {
        cout<<"[MDScaleScaner] start generate result..."<<endl;
    }
    //生成结果
    const int kRowCount = (int)p_cells.size();
    for (int rowIndex = 0; rowIndex < kRowCount; rowIndex++) {
        //行开头 ，加 [
        if (rowIndex == 0) {
            this->scanResultStr += "[";
        }
        vector<MDScaleCell> rowCells = p_cells[rowIndex];
        const int kColumnCount = (int)rowCells.size();
        for (int columnIndex = 0; columnIndex < kColumnCount; columnIndex++) {
            //列开头，加 [
            if (columnIndex == 0) {
                this->scanResultStr += "[";
            }
            MDScaleCell cell = rowCells[columnIndex];
            //每一个cell 是一个数组
            this->scanResultStr += "[";
            //计算当前列的值
            String answers = "";
            if (cell.questionCount>0 && cell.questions != NULL) {
                for (int questionIndex = 0; questionIndex < cell.questionCount; questionIndex++) {
                    MDScaleQuestion question = cell.questions[questionIndex];
                    //                    answers += "\"";
                    answers += to_string(question.value);
                    //                    answers += "\"";
                    answers += ",";
                }
                answers.pop_back();//剔除最后一个逗号
            }
            //把当前列的值 添加到结果中
            this->scanResultStr += answers;
            this->scanResultStr += "]";
            //每一列的逗号分隔符
            if (columnIndex < kColumnCount-1) {
                this->scanResultStr += ",";
            } else {
                //最后一列
                this->scanResultStr += "]";
            }
        }
        //每一行的逗号分隔符
        if (rowIndex < kRowCount-1) {
            this->scanResultStr += ",";
        } else {
            //最后一行
            this->scanResultStr += "]";
        }
    }
    //没有识别到 结果
    if (this->scanResultStr.length()<=0) {
        //没有结果，识别失败
        return false;
    }
    //没有识别到姓名
    if (p_nameMatRect.width<=0 && p_nameMatRect.height<=0) {
        return false;
    }
    //输出结果
    if (this->enableDebug) {
        cout<<"[MDScaleScaner] generate result:"<<this->scanResultStr<<endl;
    }
    //生成结果
    this->scanResultCStr = this->scanResultStr.c_str();
    if (p_originMat.data != NULL) {
        const float offsetX = p_nameMatRect.width*kMDScaleNameCropScale;
        p_nameMatRect.x = p_nameMatRect.x+this->p_outerContourRect.x+offsetX;
        p_nameMatRect.y = p_nameMatRect.y+this->p_outerContourRect.y;
        p_nameMatRect.width = p_nameMatRect.width-offsetX;
        this->nameMat = this->p_originMat(p_nameMatRect);
    }
    return true;
}

/*轮廓描边*/
cv::Mat MDScaleScaner::drawContours() {
    if (!this->enableDebug) {
        cout<<"[MDScaleScaner] scan result:"<<scanResultStr<<endl;
    }
    if (this->p_originMat.data == NULL) {
        return Mat();
    }
    Mat panelMat = this->p_originMat.clone();
    //    //校验成功后，矩形轮廓描边
    //    for (int row = 0; row < p_cells.size(); row++) {
    //        vector<MDScaleCell> rectRow = p_cells[row];
    //        for (int column = 0; column < rectRow.size(); column++) {
    //            MDScaleCell rect = rectRow[column];
    //            vector<Point> rectPoints = p_contours[rect.index];
    //            for (int i = 0; i < rectPoints.size(); i++) {
    //                Point *point = &(rectPoints[i]);
    //                (*point).x += this->p_outerContourRect.x;
    //                (*point).y += this->p_outerContourRect.y;
    //            }
    //            polylines(panelMat, rectPoints, true, Scalar(205,0,0), 2, LINE_AA);
    //            //将文本框居中绘制。默认其实位置文字左下角
    //            string text = to_string(row)+"," + to_string(column);
    //            Point origin = Point(rect.x+this->p_outerContourRect.x+rect.width/2.0-15, rect.y+this->p_outerContourRect.y+rect.height/2.0-10);
    //            putText(panelMat, text, origin, FONT_HERSHEY_COMPLEX, 1, Scalar(1), 1, FILLED, 0);
    //        }
    //    }
    
    // 画 选项矩形框
    for (int rowIndex = 0; rowIndex < p_cells.size(); rowIndex++) {
        vector<MDScaleCell> rowCells = p_cells[rowIndex];
        for (int columnIndex = 0; columnIndex < rowCells.size(); columnIndex++) {
            MDScaleCell cell = rowCells[columnIndex];
            if (cell.questions == NULL || cell.questionCount<=0) {
                continue;
            }
            for (int questionIndex = 0; questionIndex < cell.questionCount; questionIndex++) {
                MDScaleQuestion question = cell.questions[questionIndex];
                Rect rect1 = Rect(question.optionA.x+this->p_outerContourRect.x, question.optionA.y+this->p_outerContourRect.y, question.optionA.width, question.optionA.height);
                rectangle(panelMat, rect1, Scalar(1), 1, 1, 0);
                
                Rect rect2 = Rect(question.optionB.x+this->p_outerContourRect.x, question.optionB.y+this->p_outerContourRect.y, question.optionB.width, question.optionB.height);
                rectangle(panelMat, rect2, Scalar(1), 1, 1, 0);
                
                Point origin1 = Point(question.optionA.x+this->p_outerContourRect.x+question.optionA.width/2.0+15, question.optionA.y+this->p_outerContourRect.y+question.optionA.height/2.0);
                putText(panelMat, to_string(question.value), origin1, FONT_HERSHEY_COMPLEX, 1, Scalar(1), 1, FILLED, 0);
            }
        }
    }
    
    //    //不论成功失败，矩形轮廓描边
    //    map<int, vector<MDScaleCell>>::iterator iter = p_sectionMap.begin();
    //    while(iter != p_sectionMap.end())
    //    {
    //        vector<MDScaleCell> rectRow = iter->second;
    //        for (int index = 0; index < rectRow.size(); index++) {
    //            MDScaleCell rect = rectRow[index];
    //            string text = to_string(index);//to_string(iter->first)+"," +
    //            Point origin = Point(rect.x+this->p_outerContourRect.x+rect.width/2.0-15, rect.y+this->p_outerContourRect.y+rect.height/2.0-10);
    //            putText(panelMat, text, origin, FONT_HERSHEY_COMPLEX, 1, Scalar(1), 1, FILLED, 0);
    //        }
    //        iter ++;
    //    }
    return panelMat;
}

/*清除缓存、mat等等对象*/
void MDScaleScaner::clean() {
    p_outerContourArea = 0;
    p_contours.clear();
    p_hierarchys.clear();
    //释放 cells
    for (int rowIndex = 0; rowIndex < p_cells.size(); rowIndex++) {
        for (int columnIndex = 0; columnIndex < p_cells[rowIndex].size(); columnIndex++) {
            MDScaleCell cell = p_cells[rowIndex][columnIndex];
            if (cell.questions != NULL) {
                free(cell.questions);
                cell.questions = NULL;
            }
        }
    }
    p_cells.clear();
    p_sectionMap.clear();
    p_optionMap.clear();
    p_sections.clear();
    p_noSuperCells.clear();
    
    //扫描结果
    p_nameMatRect.x = 0;
    p_nameMatRect.y = 0;
    p_nameMatRect.width = 0;
    p_nameMatRect.height = 0;
    resultMat = NULL;
    scanResultCStr = NULL;
    scanResultStr.clear();
    p_questionCount = 0;
    //测试数据
    p_loopCount = 0;
}

/* 清除上次二值化的缓存 */
void MDScaleScaner::cleanThresholdCache() {
    p_contours.clear();
    p_hierarchys.clear();
    p_sectionMap.clear();
    p_optionMap.clear();
    p_sections.clear();
    p_cells.clear();
    p_noSuperCells.clear();
    if (p_cells.empty()) {
        //初始化
        p_cells.resize(kMDScaleSectionRowCount);
    }
    p_nameMatRect.x = 0;
    p_nameMatRect.y = 0;
    p_nameMatRect.width = 0;
    p_nameMatRect.height = 0;
    p_questionCount = 0;
}
