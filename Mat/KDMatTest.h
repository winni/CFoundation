//
// Created by 叶青 on 2017/2/28.
//  Mat 常用方法

#ifndef KDTEST_KDMATTEST_H
#define KDTEST_KDMATTEST_H

#include <string>
#include <opencv2/opencv.hpp>

class KDMatTest {

private:
    cv::Mat pri_mat;
    cv::Mat pri_mat_copy;
public:
    KDMatTest(void);
    KDMatTest(std::string rPath);
    ~KDMatTest(void);

    /*加载图片*/
    void loadImg(std::string rPath);

    /*浅拷贝测试*/
    void shallowCopy();
    /*深拷贝*/
    void deepCopy();
    /*release 测试*/
    void release();
    /*显示图片*/
    void show();
    /*裁剪图片*/
    void cut();
    /*显示新的mat*/
    void showNewMat();
};


#endif //KDTEST_KDMATTEST_H
