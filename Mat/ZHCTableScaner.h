//
// Created by YeQing on 2018/11/14.
// 表格扫描

#ifndef CFOUNDATION_KDSQUARES_H
#define CFOUNDATION_KDSQUARES_H

#include <string>
#include "opencv2/core/core.hpp"
#include <map>


//#ifdef DEBUG
#define kZHCTableScaner_EnableLog 1//是否启用日志
//#endif


/*矩形轮廓节点*/
typedef struct {
    int index;//对应轮廓数据的index
    float centerX;//中心点 x
    float centerY;//中心点 y
    float x;
    float y;
    float width;//宽度
    float height;//高度
    float distance;//距离图片原点的位置
}KDRectNode;

/*选项轮廓节点*/
typedef struct {
    int index;//对应轮廓数据的index
    float centerX;//中心点 x
    float centerY;//中心点 y
    float x;
    float y;
    float width;
    float height;
    bool isBlack;//是否涂黑
    bool isSecondL;//是否第二行,方便排序
}KDOptionNode;

class ZHCTableScaner {

private:
    std::vector<std::vector<cv::Point> > p_contours;//所有轮廓
    std::vector<KDRectNode > p_areaList;//矩形区域一维数组
    std::vector<std::vector<KDRectNode> > p_areaData;//矩形区域二维数组
    double p_outerContourArea;//外层矩形轮廓总面积(用来计算实际mat和实例的mat的比例)
    std::map<int,cv::Rect> p_rectContourMap;//矩形区域map，key:轮廓index,value:子选项轮廓KDOptionNode
    std::map<int,std::vector<KDOptionNode> > p_optionContourMap;//矩形区域map，key:轮廓index,value:子选项轮廓KDOptionNode
    cv::Mat p_originalMat;//原始图像
    float p_imgRatio;//图片比例
    float p_optionH;//选项轮廓高度(原本的选项地图轮廓高度,即没有涂色的时候,涂色之后,轮廓可能会变大)
    ////【测试参数】,打开 kZHCTableScaner_EnableLog 开关才会记录////
    unsigned long p_loopCount;//记录一次识别需要循环的次数
    std::vector<std::vector<cv::Point> > p_rectContours;//矩形轮廓顶点数组

    /*查找 最大外轮廓*/
    void findOuterContour(cv::Mat inputImg, cv::Mat *outputImg);

    /*查询 所有的矩形轮廓*/
    void findRectContours(cv::Mat inputImg);

    /*查找 选项子轮廓*/
    void findSubOptionContour(cv::Mat inputImg, std::vector<cv::Point> contour, int index, double area, std::vector<cv::Vec4i> hierarchys);

    /*矩形轮廓二维 排序*/
    void sortRectContours(cv::Mat inputImg);

    /*子选项轮廓排序*/
    void sortSubOptionContours(int contourIndex);

    /*获取矩形轮廓的rect*/
    cv::Rect getRectContourRect(int contourIndex);

    /*拼接扫描结果*/
    void joinScanResultString(bool isRowHead, bool isRowTail, int contourIndex, bool isTableEnd);

public:
    cv::Mat p_resultMat;//操作结果 mat
    cv::Mat nameMat;//操作结果 mat
    char* p_resultCStr;//扫描结果 字符串(c)
    std::string p_scanResultStr;//扫描结果 字符串(c++)

    ZHCTableScaner();
    ~ZHCTableScaner();

    /** 根据图片路径,开始扫描*/
    void startScan(std::string rPath);

    /** 根据mat对象,开始扫描 */
    void startScan(cv::Mat inputMat);

    /** 轮廓描边 */
    void drawContours();

    /** 清除图像、mat等等缓存 */
    void clean();

};


#endif //CFOUNDATION_KDSQUARES_H
