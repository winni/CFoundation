//
//  MDScaleScaner.h
//  ScaleScaner
//  version v1.0.0.1
//
//  Created by YeQing on 2018/12/1.
//  Copyright ? 2018年 zhihan. All rights reserved.
//  表格扫描，c++实现

#ifndef MDScaleScaner_h
#define MDScaleScaner_h

#include <string>
#include <map>

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdocumentation-deprecated-sync"
#pragma clang diagnostic ignored "-Wdocumentation"
#include "opencv2/core/core.hpp"
#pragma clang diagnostic pop

namespace ZHScaner {
    
    /// mat 方向
    enum MDMatDirection{
        Top = 0,//朝上，正常方向
        Bottom = 1,//朝下，倒置
        Left = 2,//朝左
        Right = 3//朝右
    };
    
    /// 选项 结构体
    typedef struct _MDScaleOption {
        int index;
        float x;
        float y;
        float width;
        float height;
        float centerX;//中心点 x
        float centerY;//中心点 y
        _MDScaleOption() {
            x = -1;
            x = 0;
            y = 0;
            width = 0;
            height = 0;
            centerX = 0;
            centerX = 0;
        }
    }MDScaleOption;
    
    /* 题目 结构体 */
    typedef struct _MDScaleQuestion {
        MDScaleOption optionA;
        MDScaleOption optionB;
        /// 题目选择的答案，默认-1,表示未选择
        int value;
        
        _MDScaleQuestion() {
            value = -1;//表示没有选择任何选项
        }
    }MDScaleQuestion;
    
    /* cell结构体 */
    typedef struct _MDScaleCell {
        int index;//对应轮廓数据的index
        int row;
        int column;
        float centerX;//中心点 x
        float centerY;//中心点 y
        float x;
        float y;
        float width;//宽度
        float height;//高度
        float distance;//距离图片原点的位置
        int blurred;//是否模糊，-1:未知;0:不模糊;1:模糊
        /// 题目数量，默认0
        int questionCount;
        /// 问题列表
        MDScaleQuestion *questions;
        
        _MDScaleCell() {
            index = -1;
            row = -1;
            column = -1;
            centerX = 0;
            centerY = 0;
            x = 0;
            y = 0;
            width = 0;
            height = 0;
            distance = 0;
            blurred = -1;
            questionCount = 0;
            questions = NULL;
        }
        
        ~_MDScaleCell() {
            if (questions != NULL) {
                //            free(questions);
                questions = NULL;
            }
        }
    }MDScaleCell;
    
    /* section结构体 */
    typedef struct _MDScaleSection {
        int index;//对应轮廓数据的index
        float centerX;//中心点 x
        float centerY;//中心点 y
        float x;
        float y;
        float width;//宽度
        float height;//高度
        float distance;//距离图片原点的位置
        std::vector<std::vector<_MDScaleCell>> cells;//子矩形 二维数组
    }MDScaleSection;
    
    /// 扫描器 class
    class MDScaleScaner {
    private:
        /// 外轮廓 面积(用来计算实际mat和实例的mat的比例)
        double p_outerContourArea;
        /// mat 方向,默认是 top
        MDMatDirection p_direction;
        /// 外轮廓 rect
        cv::Rect p_outerContourRect;
        /// 所有轮廓层级，index:后、前、子、父
        std::vector<cv::Vec4i> p_hierarchys;
        /// 所有轮廓点，每个轮廓包含所有的点
        std::vector<std::vector<cv::Point> > p_contours;
        /// section 列表，按顺序存储section
        std::vector<MDScaleSection> p_sections;
        /// section map，{key:section-hierarchy-index, value:rect-list}
        std::map<int, std::vector<MDScaleCell>> p_sectionMap;
        /// cell map，{key:cell-hierarchy-index, value:options-list}
        std::map<int, std::vector<MDScaleOption>> p_optionMap;
        /// 没有父层级的rect列表（rect面积合法，但是没有父层级，很少会出现这种case，即默认列表为空）
        std::vector<MDScaleCell > p_noSuperCells;
        /// cell 最终列表，按维度分组，每个维度按顺序排列
        std::vector<std::vector<MDScaleCell> > p_cells;
        /// 名称 mat 的rect
        cv::Rect p_nameMatRect;
        /// 查询结果，题目总数量
        int p_questionCount;
        
        ////【测试参数】,打开 enableDebug 开关才会记录////
        // 记录一次识别需要循环的次数
        unsigned long p_loopCount;
        /// 原始图像
        cv::Mat p_originMat;
        
        /* 查找 最大外轮廓 */
        bool findOuterContour(cv::Mat inputImg, cv::Mat *outputImg);
        /* 根据方向，旋转原始mat */
        bool rotateOriginMat(cv::Mat inputImg, cv::Mat *outputImg);
        /* 处理 最大外轮廓,查找section、cell、option */
        void processOuterContour(cv::Mat inputImg);
        /* 校验 最大外轮廓,校验section、cell 数量是否合法 */
        bool validOuterContour(cv::Mat inputImg);
        /* 排序 section 和 cell */
        bool sortSectionCells(cv::Mat inputImg);
        /* 处理 cell,选取 所有的题目 */
        bool processCells(cv::Mat inputImg);
        /* 处理 单个题目以及 选择的选项值 */
        MDScaleQuestion* processQuestion(cv::Mat inputImg, MDScaleCell* cell, int questionIndex, bool *valid);
        /* 检测 cell 是否模糊（只要当某个问题 某个选项找不到的时候，才去检测是否模糊） */
        bool checkCellBlurred(cv::Mat inputImg, MDScaleCell* cell);
        /* 通过轮廓的方式处理 单个选项，判断是否选中 */
        void processOptionByContour(cv::Mat inputImg, MDScaleOption option, bool *selected, float *optionScale);
        /* 通过ROI的方式处理 单个选项，判断是否存在，以及是否选中 */
        void processOptionByROI(cv::Mat inputImg, MDScaleOption option, bool *exsit, bool *selected, float *optionScale, MDScaleCell cell);
        /* 生成扫描结果 */
        bool generateResult();
        /* 清除上次二值化的缓存 */
        void cleanThresholdCache();
        
    public:
        /// 题目总数量，默认0，表示不校验题目总数量是否正确
        int questionCount;
        /// 测试日志开关,默认 false,不输出日志，且不记录耗时、循环次数等等
        bool enableDebug;
        /// 判断选项被选中的黑色像素点比例（取值范围0-1），默认是0.8
        float optionSelectedScale;
        /// 最小的mat size，单位kb，默认0，不检测大小。
        float minMatSize;
        /// 操作结果 mat
        cv::Mat resultMat;
        /// 姓名 mat
        cv::Mat nameMat;
        /// 扫描结果 字符串(c)
        const char *scanResultCStr;
        /// 扫描结果 字符串(c++)
        std::string scanResultStr;
        
        MDScaleScaner();
        ~MDScaleScaner();
        
        /** 根据图片路径,开始扫描*/
        bool startScan(std::string rPath);
        
        /** 根据mat对象,开始扫描 */
        bool startScan(cv::Mat inputMat);
        
        /** 测试扫描结果，轮廓描边，返回描边后的 mat */
        cv::Mat drawContours();
        
        /** 清除图像、mat等等缓存 */
        void clean();
    };
    
}
#endif /* MDScaleScaner_h */

