//
// Created by 叶青 on 2017/3/3.
//  mat图像 叠加

#include "KDMatSuperPosition.h"
#include "../Define/KDPrefixHeader.h"
#include "../Utils/KDMatHelp.h"
#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>

using namespace cv;
using namespace std;
KDMatSuperPosition::KDMatSuperPosition() {
}

KDMatSuperPosition::~KDMatSuperPosition() {
    KDDealloc();
}

/*
 * 两个mat叠加
 * g(x) = (1 - alpha)* f_{0}(x) + alpha * f_{1}(x)
 * */
void KDMatSuperPosition::superPosition(void) {
    string rPath1 = "./Res/sp_a.jpeg";
    string rPath2 = "./Res/sp_b.jpg";
    Mat matBottom = imread(rPath1, CV_LOAD_IMAGE_COLOR );
    Mat matTop = imread(rPath2, CV_LOAD_IMAGE_COLOR );
    if(!matBottom.data || !matTop.data){
        KDLog("loadImg error,img is null" );
        return ;
    }

    double alpha = 0.5;
    double beta = 1- alpha;
    Mat matDest = Mat(matBottom.size(),matBottom.type());
    const double beginTime = KDMatBeginTime();
    //1.addWeighted 实现
    addWeighted(matBottom,alpha,matTop,beta,0,matDest);//最快,耗时:8.216427 milliseconds

    //2.自己实现,mat的data指针遍历,耗时:28.355087 milliseconds
//    {
//        const int rowsBottom = matBottom.rows;
//        const int colsBottom = matBottom.cols*matBottom.channels();
//        uchar *ptrBottom = matBottom.data;
//        uchar *ptrTop = matTop.data;
//        uchar *ptrDest = matDest.data;
//        for (int i = 0; i < rowsBottom * colsBottom; ++i) {
//            *ptrDest = (*ptrBottom)*alpha+beta*(*ptrTop);
//            ptrDest++;
//            ptrBottom++;
//            ptrTop++;
//        }
//    }

    //3.自己实现,传统指针遍历,耗时:19.148927 milliseconds
    {
        const int rowsBottom = matBottom.rows;
        const int colsBottom = matBottom.cols*matBottom.channels();
        for (int i = 0; i < rowsBottom; ++i) {
            uchar *ptrBottom = matBottom.ptr<uchar>(i);
            uchar *ptrTop = matTop.ptr<uchar>(i);
            uchar *ptrDest = matDest.ptr<uchar>(i);
            for (int j = 0; j <colsBottom ; ++j) {
                ptrDest[j] = (ptrBottom[j]) * alpha + beta * (ptrTop[j]);
            }
        }
    }
    KDMatPrintTime(beginTime);

    KDMatShowImgs(&matBottom,&matTop,&matDest,NULL);
}
