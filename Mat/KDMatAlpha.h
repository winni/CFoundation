//
// Created by 叶青 on 2017/3/6.
//  Mat 透明度和对比度

#ifndef KDTEST_KDMATALPHA_H
#define KDTEST_KDMATALPHA_H


class KDMatAlpha {
public:
    KDMatAlpha();

    virtual ~KDMatAlpha();

    void changeAlpha();
};


#endif //KDTEST_KDMATALPHA_H
