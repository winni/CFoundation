//
// Created by 叶青 on 2017/3/6.
//  Mat 透明度和对比度

#include "KDMatAlpha.h"
#include "../Utils/KDMatHelp.h"
#include "../Define/KDPrefixHeader.h"
#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>

using namespace cv;
using  namespace std;

KDMatAlpha::KDMatAlpha() {}

KDMatAlpha::~KDMatAlpha() {
    KDDealloc();
}

void KDMatAlpha::changeAlpha() {
    string path = "./Res/sp_a.jpeg";
    Mat mat = imread(path,CV_LOAD_IMAGE_COLOR);
    KDMatPointEnum(&mat,[&mat](bool *stop, int row, int col, uchar *point){
        *point = saturate_cast<uchar>(0.5*(*point) + 100);
    });
    KDMatShowImg(mat);
}
