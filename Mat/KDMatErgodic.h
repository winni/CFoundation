//
// Created by 叶青 on 2017/3/1.
//  mat 遍历

#ifndef KDTEST_KDMATERGODIC_H
#define KDTEST_KDMATERGODIC_H

#include <string>
#include <opencv2/opencv.hpp>

class KDMatErgodic {

private:
    cv::Mat pri_mat;
public:
    KDMatErgodic(void);
    KDMatErgodic(std::string rPath);
    ~KDMatErgodic(void);

    /*扫描图像,颜色缩减运算*/
    void scanImg();
};


#endif //KDTEST_KDMATERGODIC_H
