//
// Created by 叶青 on 2017/3/1.
// Mat 遍历

#include "KDMatErgodic.h"
#include <opencv2/highgui.hpp>
#include "../Utils/KDMatHelp.h"
#include "../Define/KDPrefixHeader.h"

using namespace std;
using namespace cv;

Mat& reducePixelC(Mat& mat, const uchar* const table);
Mat& reducePixelC2(Mat& mat, const uchar* const table);
Mat& reducePixelIterator(Mat& mat, const uchar* const table);
Mat& reducePixelRandomAccess(Mat& mat, const uchar* const table);
Mat& reducePixelLutFun(Mat& mat, const uchar* const table);

KDMatErgodic::KDMatErgodic() {
}

KDMatErgodic::KDMatErgodic(std::string rPath) {
    pri_mat = imread(rPath, CV_LOAD_IMAGE_COLOR );
    if(!pri_mat.data){
        KDLog("loadImg error,img is null");
        return ;
    }
//    cout << pri_mat << endl;
}

KDMatErgodic::~KDMatErgodic() {
    pri_mat.release();
    KDDealloc();
}

void KDMatErgodic::scanImg() {
    int divideWith = 30;
    //原始颜色表,进行颜色缩减 【0-divideWith:使用0; divideWith-2*divideWith:使用divideWith...】
    uchar table[256];
    for (int i = 0; i < 256; ++i) {
        table[i] = divideWith * (i / divideWith);
    }
    double begintTime = KDMatBeginTime();

//    for (int i = 0; i < times; ++i) {
//        Mat copyObj = pri_mat.clone();
//        Mat J = ScanImageAndReduceC(copyObj, table);
//    }

    Mat copyObj = pri_mat.clone();
//    Mat J = reducePixelC(copyObj, table);//耗时0.0265365 milliseconds
//    Mat J = reducePixelC2(copyObj, table);//耗时0.025353 milliseconds
//    Mat J = reducePixelIterator(copyObj, table);//耗时0.154591 milliseconds.
//    Mat J = reducePixelRandomAccess(copyObj, table);//耗时0.160375 milliseconds.
    Mat J = reducePixelLutFun(copyObj, table);//耗时0.0107087 milliseconds.

    KDMatPrintTime(begintTime);

    KDMatShowImgs(&pri_mat,&copyObj,NULL);

    waitKey();
}


#pragma mark - private

Mat& reducePixelC(Mat& mat, const uchar* const table)
{
    // accept only char type matrices
    CV_Assert(mat.depth() != sizeof(uchar));

    int channels = mat.channels();

    int nRows = mat.rows ;//* channels;
    int nCols = mat.cols *channels;

    if (mat.isContinuous())
    {
        nCols *= nRows;
        nRows = 1;
    }

    int i,j;
    uchar* p;
    for( i = 0; i < nRows; ++i)
    {
        p = mat.ptr<uchar>(i);
        for ( j = 0; j < nCols; ++j)
        {
            p[j] = table[p[j]];
        }
    }
    return mat;
}

Mat& reducePixelC2(Mat& mat, const uchar* const table)
{
    // accept only char type matrices
    CV_Assert(mat.depth() != sizeof(uchar));
    int channels = mat.channels();
    int nRows = mat.rows ;//* channels;
    int nCols = mat.cols *channels;
    if (mat.isContinuous()) {
        nCols *= nRows;
        nRows = 1;
    }

    int i,j;
    uchar* p = mat.data;
    for( i = 0; i < nRows*nCols; ++i) {
        *p = table[(*p)];
        p++;
    }
    return mat;
}

Mat& reducePixelIterator(Mat& mat, const uchar* const table)
{
    // accept only char type matrices
    CV_Assert(mat.depth() != sizeof(uchar));
    switch (mat.channels()){
        case 1:
        {
            MatIterator_<uchar> itBegin , itEnd;
            for (itBegin = mat.begin<uchar>(),itEnd = mat.end<uchar>(); itBegin != itEnd; itBegin++) {
                *itBegin = table[(*itBegin)];
            }
        }
            break;
        case 3:
        {
            MatIterator_<Vec3b> itBegin , itEnd;
            for (itBegin = mat.begin<Vec3b>(),itEnd = mat.end<Vec3b>(); itBegin != itEnd; itBegin++) {
                (*itBegin)[0] = table[((*itBegin)[0])];
                (*itBegin)[1] = table[((*itBegin)[1])];
                (*itBegin)[2] = table[((*itBegin)[2])];
            }
        }
            break;
        default:
            break;
    }
    return mat;
}

Mat& reducePixelRandomAccess(Mat& mat, const uchar* const table)
{
    // accept only char type matrices
    CV_Assert(mat.depth() != sizeof(uchar));
    switch(mat.channels())
    {
        case 1:
        {
            for( int i = 0; i < mat.rows; ++i)
                for( int j = 0; j < mat.cols; ++j )
                    mat.at<uchar>(i,j) = table[mat.at<uchar>(i,j)];
            break;
        }
        case 3:
        {
            Mat_<Vec3b> matTemp = mat;
            for( int i = 0; i < mat.rows; ++i)
                for( int j = 0; j < mat.cols; ++j )
                {
                    matTemp(i,j)[0] = table[matTemp(i,j)[0]];
                    matTemp(i,j)[1] = table[matTemp(i,j)[1]];
                    matTemp(i,j)[2] = table[matTemp(i,j)[2]];
                }
            mat = matTemp;
            break;
        }
    }

    return mat;
}

Mat& reducePixelLutFun(Mat& mat, const uchar* const table)
{
    CV_Assert(mat.depth() != sizeof(uchar));

    Mat matLut = Mat(1, 256, CV_8U);
    uchar* p = matLut.data;
    for( int i = 0; i < 256; ++i)
        p[i] = table[i];

    Mat matTemp;
    LUT(mat, matLut, matTemp);
    mat = matTemp;
    return mat;
}