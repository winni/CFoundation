//
// Created by 叶青 on 2017/3/6.
//  Mat 画图

#ifndef KDTEST_KDMATDRAW_H
#define KDTEST_KDMATDRAW_H


class KDMatDraw {
public:
    KDMatDraw();

    virtual ~KDMatDraw();

    void draw();
};


#endif //KDTEST_KDMATDRAW_H
