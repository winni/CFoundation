//
// Created by 叶青 on 2016/12/16.
//

#include "KDFoundationTest.h"
#include "../Model/KDTestModel.h"
#include "../Utils/KDCommonHelp.h"
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

//传参实际上是拷贝一个副本,值传递,副本和入参指向同一块内存
void changePoint(int *p){
    *p=10;//修改副本指针指向的内存变量  的 内容
}

void changePointRef(int *p){
//    *p= 20;//直接修改内容

    p=new int;//修改副本指针指向,对入参无效
    *p= 20;
}

void changePointRef2(int **p){
//    **p=20;//直接修改内容

    *p=new int;//修改副本指针指向,但是副本(二级指针)和入参(二级指针)指向的地址都是 p指针的地址
    **p = 20;
}

//传递p指针,引用方式传递(c++特性,c只有值传递)
void changePointRef3(int *&p){
//    *p=20;//直接修改内容,

    p=new int;
    *p = 20;
}


KDFoundationTest::KDFoundationTest() {
    cout << "KDFoundationTest init" << endl;
}
KDFoundationTest::~KDFoundationTest() {

}

/*测试while循环*/
void KDFoundationTest::testWhile() {
    char a[10],b[5];
    cin >> a;
    cin >> b;
    cout << "a:" << a << "\nb:" << b << endl;

    int64_t  i=50;
    int64_t  sum=0;
    while(i<=100){
        sum+=i;
        i++;
    }
    cout << "sum:" << sum << endl;
}

/*测试cin 输入*/
void KDFoundationTest::testCin() {
    string value="";
    string  sum="";
    while(getline(cin,value)){
        if(value.size()>0){
            sum+=value;
        }
    }
    cout << "\nsum:" << sum << endl;
}

/*测试string*/
void KDFoundationTest::testString() {
    double currentTime1 = KDBeginTime();
    string a="aa";
    string b="bb";
    string sum="";
    for (int i = 0; i < 10000; i++) {
        sum.append(a);
        sum.append(b);
//        sum+=a+b;//效率低
//        sum=sum+a+b;//效率更低
    }
    KDPrintTime(currentTime1);
    cout << "\nsum:" << sum << endl;
}

/*测试class*/
void KDFoundationTest::testClass() {
    KDPeople *m1=NULL;
    m1 = new KDPeople();
    cout << m1->age << endl;
}

/*测试指针和引用*/
void KDFoundationTest::testPointAndRefrence() {
    int i=10;
    int j=20;
    int &icopy=i;
    i=20;
    icopy=11;
//    &icopy=j;//引用关系不能更改
    int *icopy1 = &j;
    j=30;
//    icopy1 = &i;//指针指向可以更改
    *icopy1 = 0;
    cout << "icopy:" << icopy  << ",i:" << i<< endl;
    cout << "\nicopy1:" << *icopy1 << ",j:" << j << endl;
}

/*测试void指针*/
void KDFoundationTest::testVoidPoint() {
    int c = 10;
    int *a = nullptr;
    int *b = NULL;
    int *d = &c;
    void *e= &c;
    *d=11;
//    *e=12;
    cout << "d:" << *d;
//    cout << "e:" << *e;
}

/*测试指针引用*/
void KDFoundationTest::testPointRefrence() {
    int a=1;
    int *b=&a;
    cout << "b:" << *b  <<  ",a:" << a << endl;
//    changePoint(b);//修改指针的值有效
//    changePointRef(b);//修改指针指向无效
//    changePointRef2(&b);//修改指针的值有效
    changePointRef3(b);//修改指针的值有效
    cout << "b:" << *b <<  ",a:" << a <<endl;
}

/*测试常量指针:指针指向const变量*/
void KDFoundationTest::testPointConst() {
    const int a =10;
    int a1 = 20;
//    int *b = &a;//指针也必须是const
    const int *c = &a;
    cout << "c:" << *c << endl;
//    *c=20;//指针指向的是const变量
    c=&a1;
    cout << "c:" << *c << endl;
    a1=30;
    cout << "c:" << *c << endl;
}

/*测试指针常量:const指针*/
void KDFoundationTest::testConstPoint() {
    int a = 10;
    int a1 = 20;
    int *const b = &a;
//    *b = &a1;//const 指针不能修改指向
    cout << "b:" << *b << endl;
    a = 11;
    cout << "b:" << *b << endl;
    *b = 12;//可以修改指针的指向的内容值
    cout << "b:" << *b << endl;
}

/*测试顶层const、底层const*/
void KDFoundationTest::testTopConst() {

    //顶层const（top-level const）表示指针（或引用等）本身是个常量。
    // 底层const（low-level const）表示指针指的对象是一个常量。
    const int i = 0;//i 表示是一个常量整型,i是不能够改变的，因此它是一个顶层const
    int j =1;
    int *const p1 = &j;//表示指针p1本身是不可以改变的，这是一个顶层const，表示p1将就永远指向j
    const int *p2 = &i;//表示p2指向的是一个常量整型，而p2本身是可以改变的，这是一个底层const
    const int *p3 = &j;//也是正确的，p2只是自觉地不会改变它指向的值，这只是它的一相情愿罢了
}

void KDFoundationTest::testDeclType() {
    int a = 1;
    int *a1 = new int;
    *a1 = 10;
    decltype(a) b;
    decltype((a)) &c = b;//左值运算
    decltype(*a1) d = b;//左值运算
    cout << "a:" << a << endl;
    cout << "b:" << a << endl;
    cout << "c:" << a << endl;
    cout << "d:" << a << endl;
}

void KDFoundationTest::testVector() {
    vector<string> a;
    string b;
    while(cin >> b){
      a.push_back(b);
    }
    for(auto it = a.begin();it != a.end(); ++it){
        transform((*it).begin(), (*it).end(), (*it).begin(), ::toupper);
        cout << *it << endl;
    }

    //赋值
    vector<int> d(10);//初始化长度 10
    for (auto i = d.begin();i != d.end();++i) {
        *i = 10;
    }

    vector<int> e(10,1);//初始化10个1

    vector<int> f;//测试insert
    vector<int>::iterator ittemp = f.begin();
    f.insert(ittemp,1);
}

void KDFoundationTest::testArray() {
    int a[] = {12,12,23324,12};
    string b[] = {"12","asda"};
    char c[5] = {'2','1','3','4','\0'};
    char c1[4] = "213";//这种初始化方式,结尾会保留一个空字符的位置
    size_t clen = strlen(c);//数组c 没有 以空字符'\0'串结尾,strlen 计算出来的长度是不正确的

    int length = sizeof(c1) / sizeof(char);
    cout << "c1 size:" << length << endl;

    int *d = new int();
    *d = 1;
    int *d1[2] = {d,new int};//从右向左读,d1是一个10个元素的数组,每一项都是一个int *
    *(d1[1]) = 2;//修改第二项的值
    cout << "d1[1]:" << *(d1[1]) << endl;

    int e[] = {1,2,3,4,5};
    int (*e1)[5] = &e;//从里向外读,e1是一个指针,指向一个5个元素的数组,每一项都是int
    (*e1)[1] = 6;//修改第二项的值
    cout << "e1[1]:" << (*e1)[1] << endl;

    int f[] = {10,11,12};
    int (&f1)[3] = f;//从里向外读,f1是一个引用,指向一个5个元素的数组,每一项都是int
    f1[1] = 21;//修改第二项的值
    cout << "f1[1]:" << f1[1] << endl;

    int *g[] = {new int,new int,new int,new int};//定义一个数组,四项,每一项都是int *
    int * (&g1)[4] = g;//从里向外读;从右向左读,g1是一个数组引用,数组有四项元素,每一项都是int *
    *(g1[1]) = 2;//修改第二项的值
    cout << "g1[1]:" << *(g1[1]) << endl;

    int h[] = {1,2,3,4,5,6,7};
    for (int *h1 = h;h1 != end(h);++h1) {
        //指针迭代器
        cout << *h1 << endl;
    }

    int *h2 = h;//指向数组的第一个元素
    cout << h2[1] << endl;//h2[1] = *(h2+1)

    //循环输出数组里的元素
    constexpr  int isize = 5;
    int i[isize] = {1,2,3,4,5};
    for (int *iptr = i,ix= 0 ; ix != isize; ++ix, ++iptr) {
        cout << *iptr << endl;
    }

}

void KDFoundationTest::testCharFun() {
    //strlen 长度
    char a[5] = {'2','1','3','4'};
    char a1[4] = "213";//这种初始化方式,结尾会保留一个空字符的位置
    size_t alen = strlen(a);//数组c 没有 以空字符'\0'串结尾,strlen 计算出来的长度是不正确的

    //strcmp 比较
    char b[] = "hello world";
    char b1[] = "hello World";
    int bcmp = strcmp(b,b1);//相等,返回0;前面大,返回大于0

    //strcat 追加
    char c[] = "hello c";
    strcat(c,"plus");

    //strcpy 拷贝
    char d[] = "hello c plus plus";
    char d1[30] = "";
    strcpy(d1,d);
}

void KDFoundationTest::testArrays() {
    int a[2][3] = {1,2,3,4};

}

void KDFoundationTest::testType() {
    char a = 'a';
    int a1 = -20;
    long a2 = 2L;
    float a3 = 3.14;
    double a4 = 4.156;
    unsigned int a5 = 5;

    //测试类型
    cout << "char:" << sizeof(a) << endl;
    cout << "int:" << sizeof(a1) << endl;
    cout << "long:" << sizeof(a2) << endl;
    cout << "float:" << sizeof(a3) << endl;
    cout << "double:" << sizeof(a4) << endl;
    cout << "unsigned int:" << sizeof(a5) << endl;

    //测试运算,有符号的转成无符号:2^32-1+(-20) =  4294967296 -20 = 4294967276
    //有符号数转换为无符号数时，负数转换为大的正数，相当于在原值上加上2的n次方，而正数保持不变。
    cout << "int + unsigned int:" << (unsigned int)a1 << endl;
    cout << "int + unsigned int:" << a1 + a5 << endl;
}

struct kdlinkNode{
    int data;
    struct kdlinkNode * nextNode;
};


typedef struct kdlinkNode KDLinkNodeTest;

/*创建链表*/
KDLinkNodeTest * testLinkListCreate(KDLinkNodeTest *&linkNode) {
    KDLinkNodeTest *head,*endTmp;
    int data;
    head = (KDLinkNodeTest *)malloc(sizeof(KDLinkNodeTest));
    if(head == NULL){
        return NULL;
    }
    //初始化
    memset(head,0,sizeof(KDLinkNodeTest));
    endTmp = head;
    //读取输入
    while(scanf("%d",&data)){
        if(data<=0){
            break;
        }
        //每次输入一项,就malloc一个node
        KDLinkNodeTest *tmpNode = (KDLinkNodeTest *)malloc(sizeof(KDLinkNodeTest));
        if(tmpNode == NULL){
            continue;
        }
        memset(tmpNode,0,sizeof(KDLinkNodeTest));
        tmpNode->data = data;
        endTmp->nextNode = tmpNode;//生成的Node 往后面插入
        endTmp = tmpNode;//保存 最后的Node
    }
    linkNode = head;
    return linkNode;
}


void KDFoundationTest::testLinkList() {
    KDLinkNodeTest *linkNode;
    testLinkListCreate(linkNode);
    int i=0,j=0;
    j = i++;
}