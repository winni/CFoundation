//
// Created by 叶青 on 2016/12/16.
//

#ifndef KDTEST_KDFOUNDATIONTEST_H
#define KDTEST_KDFOUNDATIONTEST_H


class KDFoundationTest {

public:
    KDFoundationTest();
    ~KDFoundationTest();

    /*测试while循环*/
    void testWhile();
    /*测试cin*/
    void testCin();
    /*测试string append、+ 效率*/
    void testString();
    /*测试class*/
    void testClass();
    /*测试指针、引用*/
    void testPointAndRefrence();
    /*测试void 指针*/
    void testVoidPoint();
    /*测试指针引用*/
    void testPointRefrence();
    /*测试指针指向const变量*/
    void testPointConst();
    /*测试const 指针*/
    void testConstPoint();
    /*测试顶层const、底层const*/
    void testTopConst();
    /*测试类型推断*/
    void  testDeclType();
    /*测试向量:泛型*/
    void testVector();
    /*测试数组*/
    void testArray();
    /*测试标准char函数*/
    void testCharFun();
    /*测试多维数组*/
    void testArrays();
    /*测试链表*/
    void testLinkList();
    /*类型测试*/
    void testType();
};


#endif //KDTEST_KDFOUNDATIONTEST_H
